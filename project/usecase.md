***
# Marc Antonio-Sebastian
# Grupa 30231
***



# BusApp #

## Use-Case Model ##

## Version <1.0> ##



### Table of Contents ###


##### 1. Use-Case Identification #####

##### 2. Use-Case Diagrams #####



## Use-Case Model ##

#### 1. Use-Case Identification ####

Use-Case Goal: Activare locaţie

Level: User-goal level

Primary actor: Pasager din autobuz

Main succes scenario: Pasagerul îşi activează locaţia, care este transmisa unui server de unde poate fi citită de restul utilizatorilor

Extensions: Locaţia nu poate fi transmisă datorită unor defecţiuni ale GPS-ului


Use-Case Goal: Citire stare autobuz

Level: User-goal level

Primary actor: Călător care aşteaptă autobuzul

Main succes scenario: Persoana în cauză, verifică starea autobuzului căutat şi citeşte informaţiile legate de autobuzul respectiv

Extensions: Locaţia nu este exactă datorită unor defecţiuni ale GPS-ului sau datorită faptului că nu există utilizatori prezenţi în
autobuzul respectiv


Use-Case Goal: Salvare autobuze

Level: User-goal level

Primary actor: Utilizator

Main succes scenario: Utilizatorul salvează autobuzele preferate într-o bază de date locală

Extensions: Baza de date nu poate fi salvată datorită memoriei insuficiente a telefonului



#### 2. Use-Case Diagrams ####

![Alt text](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/c7b55448e00609d896abc36004079e5fcd4a714c/project/uml_busapp.JPG?token=01b86af695a0649aa36db6c1ef85e5110916b1d6)
