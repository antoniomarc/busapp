***
# Marc Antonio-Sebastian
# Grupa 30231
***



# BusApp #

## Supplementary specification ##

## Version <1.0> ##



### Table of Contents ###

#### 1. Introduction ####

#### 2. Non-functional requirements ####

##### 2.1 Availability #####

##### 2.2 Performance #####

##### 2.3 Security #####

##### 2.4 Testability #####

##### 2.5 Usability #####

#### 3. Design constraints ####


### Supplementary specification ###

#### 1. Introduction ####

Acest document conţine toate informaţiile legate de funcţionalităţile aplicaţiei BusApp, care nu au fost speficate în documentul Use-Case Model.

#### 2. Non-functional requirements ####

##### 2.1 Availability #####

Aplicaţia va fi disponibilă non-stop pentru utilizatori, atât timp cât server-ul pe care sunt stocate locaţiile autobuzelor va funcţiona.

##### 2.2 Performance #####

Aplicaţia permite până la 100 de utilizatori care îşi activează locaţia simultan.

##### 2.3 Security #####

Aplicaţia este 100% sigură, prezenţa utilizatorilor maliţioşi nu este posibilă, deoarece utilizatorii nu au nevoie să stocheze 
anumite date personale, ci doar locaţia lor care este anonimă.

##### 2.4 Testability #####

Aplicaţia este testabilă cu un efort minim pentru puţine cazuri de test, dar este greu de testat pentru mulţi utilizatori

##### 2.5 Usability #####

Aplicaţia este user-friendly şi necesită cunoştinţe minime de utilizare a unui smartphone

#### 3. Design constraints ####

Aplicaţia este implementată folosind limbajul Java si API-ul de la Android, folosind mediul de dezvoltare Android Studio, sistemul de management
a bazelor de date SQLite pentru stocare locală, respectiv FireBase pentru stocare pe server a locaţiilor utilizatorilor într-o bază de date NoSQL.
Aplicaţia va folosi librăria GoogleDirections (link în referinţe).




