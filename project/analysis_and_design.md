# Project Specification
Implementarea unei aplicaţii folosind Java + Android API, care să ofere informaţii în timp real despre autobuze folosind locaţia userilor şi care să permită
vizualizarea acestor informaţii pe hartă. De asemenea useri au posibilitatea de a stoca rutele preferate şi de a da review-uri la anumite rute.
Aplicaţia trebuie să folosească arhitectura client-server, respectiv un serviciu extern. Locaţiile userilor vor fi stocate într-o bază de date
pe server, iar rutele într-o bază de date locală.

## Elaboration – Iteration 1.1

### Domain Model
Domain Model-ul reprezintă o abstractizare la nivel de clase a obiectelor folosite în aplicaţie, precum userii, autobuzele, rutele, staţiile.
Fiecare astfel de obiect din lumea reala având ca şi corespondent o clasă din aplicaţie. Relaţiile dintre aceste clase se pot vedea în diagrama
de clase conceptuală de mai jos:

![UML Diagram](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/69493135e2de8970c99b52616978f333dbd37e7a/project/diagrams/project_umldiagram.JPG?token=bc4907828190fed0ec8f61e45d743fe70e9efb92)
### Architectural Design

#### Conceptual Architecture
Arhitectura sistemului se bazează pe pattern-ul  arhitectural Model-View-Controller. Motivatia din spatele acestei alegeri constă în faptul că
acest pattern stă la baza structurii aplicaţiilor Android, fiecare aplicaţie poate fi modelată astfel direct, modificări asupra acestei 
structuri fiind dificile de făcut. Componenta Model este compusă din clasele care formează partea de business logic a aplicaţiei, respectiv
obiectele modelate şi care vor fi prelucrate de către aplicaţie, partea de View este compusă din componentele UI, respectiv din layout-ul 
aplicaţiei, implementat folosind XML, iar Controller-ul aplicaţiei este format din clase de tip Activity, care fac legătura între Model şi View.

#### Package Design

![Package Diagram](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/69493135e2de8970c99b52616978f333dbd37e7a/project/diagrams/project_packagediagram.JPG?token=c21e5011c4ebaecaed33485e745d641b02244ced)

#### Component and Deployment Diagrams

##### Componenet Diagram

![Component Diagram](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/69493135e2de8970c99b52616978f333dbd37e7a/project/diagrams/project_componentdiagram.JPG?token=f0d466ceffa6925d75d90f4be974da2f84b7d3cb)

##### Deployment Diagram

![Deployment Diagram](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/69493135e2de8970c99b52616978f333dbd37e7a/project/diagrams/project_deploymentdiagram.JPG?token=87694960b9bc8de574627683647bf639c97780a1)

## Elaboration – Iteration 1.2

### Design Model

#### Dynamic Behavior

##### Viewing buses on map

###### Communication Diagram

![Communication Diagram1](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/4fef6b70fc18c0737f115f1800d497209ce50051/project/diagrams/project_communication1.JPG?token=c92ee0320c7bf2d3646df133285188160618c43e)

###### Sequence Diagram

![Sequence Diagram1](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/4fef6b70fc18c0737f115f1800d497209ce50051/project/diagrams/project_sequence1.JPG?token=961a1a9615fb320bc2e10fa23266186e3dc9461a)

##### Activating location

###### Communication Diagram

![Communication Diagram2](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/4fef6b70fc18c0737f115f1800d497209ce50051/project/diagrams/project_communication2.JPG?token=d16937a2fe4cf11754dcb1d4e7bb3f562343858f)

###### Sequence Diagram

![Sequence Diagram2](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/4fef6b70fc18c0737f115f1800d497209ce50051/project/diagrams/project_sequence2.JPG?token=cb74f80a558f3c564d7127a3aa2342781d2d38ae)

#### Class Design

##### UML Class Diagram

![UML Class Diagram](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/597d3dc255d1336d616210193a8ec9c0cf0de8cb/project/diagrams/busapp_uml.jpg?token=a16a02c9a19aeb92173a9a8085d67a0ab2538dd6)

Aplicaţia foloseşte Factory Pattern pentru generarea rapoartelor PDF şi CSV. Acest pattern este folosit pentru a reduce cuplajele între
aplicaţie şi clasa Report, această abstractizare fiind făcută prin intermediul acestui pattern.

#### Data Model
Modelul folosit în proiect este de tipul object-relational model şi constă în maparea claselor la obiecte JSON dintr-o bază de date NoSQL
aflată pe serverul Firebase. Acest model oferă avantajele ambelor domenii: procesarea datelor din baza de date este făcută folosind Android
API, iar persistenţa, scalabilitatea, respectiv manipularea datelor în timp real este oferită de către baza de date.

#### Unit Testing
Metodele de testare folosite au fost oferite de către librăria Robolectric, testele au constat în testarea controller-ului (claselor Activity)
prin afişarea diferitelor ferestre, respectiv prin testarea anumitor use case-uri precum: adăugare user, ştergere user, actualizare informaţii
user.

## Elaboration – Iteration 2

### Architectural Design Refinement
#### Package diagram
![Final Package Diagram](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/61afc52b2ae8452f439e01031ac7b21a6b145c0f/project/diagrams/busapp_final_packagediagram.png?token=2087046790fcb284c1f165be81657f64bd73a27d)

#### Component diagram
![Final Component Diagram](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/61afc52b2ae8452f439e01031ac7b21a6b145c0f/project/diagrams/busapp_final_componentdiagram.png?token=2391e2cfdd1494d5c0840d3556c52f45ef574687)

#### Deployment diagram
![Final Deployment Diagram](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/69493135e2de8970c99b52616978f333dbd37e7a/project/diagrams/project_deploymentdiagram.JPG?token=87694960b9bc8de574627683647bf639c97780a1)

### Design Model Refinement
![Final UML Diagram](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/61afc52b2ae8452f439e01031ac7b21a6b145c0f/project/diagrams/busapp_final_umldiagram.bmp?token=bae41d4084a83e1cdbf5ae4b0c30e3bcd4195036)


## Construction and Transition

### System Testing
Pentru a testa sistemul, am folosit atât unit teste cât şi testare efectivă folosind emulatorul şi un dispozitiv mobil real. Cazurile de
testare au reprezentat testarea afişării diferitelor ferestre, respectiv testarea anumitor use case-uri precum: adăugare user, actualizare
informaţii user, ştergere user.

### Future improvements
Dezvoltări ulterioare ale sistemului pot consta într-un sistem de review al userilor care îşi activează locaţia, astfel useri care îşi
activează de multe ori locaţia în mod corect, pot primi diferite premii. Alte dezvoltări ar putea consta în îmbunătăţirea interfeţei grafice,
afişarea pe hartă a rutei autobuzului. Algoritmi de verificare a locaţiei utilizatorului, pentru a scade marja de eroare comisă datorită
activării locaţiei din alte zone, etc.

## Bibliography
- [Markdown online editor](http://dillinger.io/)
- [Markdown documentation](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)