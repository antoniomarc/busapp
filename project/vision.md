***
# Marc Antonio-Sebastian
# Grupa 30231
***



# BusApp #

## Vision ##

## Version <1.0> ##



### Table of Contents ###

#### 1.	Introduction	4   #####

##### 1.1.	Purpose	4

##### 1.2.	Scope	4

##### 1.3.	Definitions, Acronyms, and Abbreviations	4

##### 1.4.	References	4

#####1.5.	Overview	4

#### 2.	Positioning	4   #####

##### 2.1.	Problem Statement	4

##### 2.2.	Product Position Statement	4

#### 3.	Stakeholder and User Descriptions	5 #####

##### 3.1.	Stakeholder Summary	5

##### 3.2.	User Summary	5

##### 3.3.	User Environment	6

#### 4.	Product Requirements	6 #####


### Vision ###

#### 1. Introduction  #####

##### 1.1 Purpose

Scopul acestui document este de a analiza şi de a defini cerinţele şi funcţionalităţiile aplicaţiei mobile BusApp. Descrie capabilităţile 
cerute de către utilizatori. Detaliile despre cum îşi propune aplicaţia să acopere cerinţele utilizatorilor sunt descrise în specificaţiile
Supplementary şi Use Case.

##### 1.2 Scope

Acest document face referire la aplicaţia BusApp. Aplicaţia va rula pe platforma Android şi va rezolva problema legată de starea curentă 
a autobuzelor de pe un anumit traseu, dând posibilitatea utilizatorilor de a afla dacă autobuzul lor se află în întârziere sau nu.

##### 1.3 Definitions, Acronyms, and Abbreviations

Android - Sistem de operare de la Google care rulează pe smartphone-uri
Crowdsourcing - Aplicaţia se foloseşte de utilizatorii săi pentru a primi date
Google Maps - Serviciu de la Google, folosit pentru afişarea hărţilor
    
##### 1.4 References

[GoogleDirections Library for Android](https://github.com/akexorcist/Android-GoogleDirectionLibrary)

[Firebase](https://firebase.google.com/)

##### 1.5 Overview

Documentul mai conţine informaţii despre problema propusă şi soluţionată de către aplicaţie, cerinţele utilizatorilor şi ale produsului.


#### 2. Positioning #####

##### 2.1 Problem statement

   
| Problema                  | a aştepta un autobuz în staţie o durată mai mare de timp observând că autobuzul respectiv nu mai soseşte                                                                                                                                                                                           |
|---------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| afectează                 | persoanele care stau în staţie aşteptând autobuzul care întârzie                                                                                                                                                                                                                                   |
| impactul fiind            | acela de a nu ştii situaţia curentă a autobuzului şi timpul rămas până când acesta va ajunge în staţie                                                                                                                                                                                             |
| o soluţie de succes ar fi | o aplicaţie mobilă simplă care poate să arate în timp real locaţia autobuzelor pe hartă, folosind GPS-ul,integrat în smartphone-urile utilizatorilor, aceştia beneficiind astfel de toate informaţiile legate de starea autobuzului, respectiv,de autobuz în sine, ca de exemplu numele, ruta etc. |

##### 2.2 Product statement


| Pentru            | persoanele care circulă cu autobuzul                                                                                                                                                                                                        |
|-------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Care              | întârzie, iar utilizatorii doresc să afle poziţia curentă a acestuia                                                                                                                                                                        |
| BusApp            | reprezintă o aplicaţie pe Android                                                                                                                                                                                                           |
| Care              | propune o rezolvare cât mai de încredere a problemei localizării autobuzelor în timp real                                                                                                                                                   |
| Faţă de           | aplicaţiile aflate la ora actuală pe piaţă care nu oferă o soluţie sigură pentru această problemă,bazându-se pe timpul estimativ la care va ajunge autobuzul într-o anumită staţie fără să ţină cont de anumite,situaţii excepţionale       |
| Această aplicaţie | foloseşte GPS-ul utilizatorilor pentru a determina în timp real locaţia autobuzelor, bazându-se,pe crowdsourcing, pe lângă alţi algoritmi care reduc marja de eroare care ar putea apărea; totodată afişează date,despre autobuzele căutate |

    
#### 3. Stakeholder and user description


##### 3.1 Stakeholder summary


| Name               | Description                         | Responsabilities                                                                                                                             |
|--------------------|-------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------|
| Software Architect | Se ocupă de dezvoltarea proiectului | Responsabil de arhitectura generală a sistemului, de îndeplinirea cerinţelor funcţionale şi non funcţionale şi de implementarea propriu zisă |


##### 3.2 User summary


| Name                                                  | Description                           | Responsabilities                                                                                    | Stakeholder |
|-------------------------------------------------------|---------------------------------------|-----------------------------------------------------------------------------------------------------|-------------|
| Persoane care călătoresc cu autobuzul                 | Principalii utilizatori ai aplicaţiei | Folosesc aplicaţia pentru a afla locaţia în timp real a autobuzelor, respectiv date despre autobuze | Ei însuşi   |
| Persoane care doresc să cunoască traficul autobuzelor | Utilizatori secundari ai aplicaţiei   | Folosesc aplicaţia pentru a afla locaţia în timp real a autobuzelor, respectiv date despre autobuze | Ei însuşi   |


##### 3.3 User environment


1. Aplicaţia BusApp va fi folosită de persoanele care călătoresc cu autobuzul, acestea pot fi împărţite în mai multe categorii:

* Persoane care se află într-un autobuz înregistrat în aplicaţie
* Persoane care aşteaptă autobuzul sau care doresc să afle date despre un anumit autobuz

2. Persoanele care se află într-un autobuz înregistrat în aplicaţie, pot furniza locaţia lor, specificând numele şi staţia în care se află, pentru
ca aplicaţia să poată localiza exact autobuzul în care se află

3. Persoanele care aşteaptă autobuzul sau care doresc să afle date despre un anumit autobuz au posibilitatea de a vedea pe hartă autobuzele
selectate după un anumit criteriu, respectiv să vadă date despre autobuze privind numele, ruta, viteza estimativă.

4. Persoanele pot salva autobuzele preferate, pentru a primi mai uşor informaţii despre ele


#### 4. Product requirements

Aplicaţia BusApp trebuie să atingă standardele aplicaţiilor curente de verificare şi afişare a datelor legate de autobuze, să ruleze pe orice
smartphone care are la bază sistemul de operare Android, versiunea 4.4 (minim). Utilizatorul trebuie să aibă GPS-ul telefonului funcţional sau să
aibă conexiune la internet.