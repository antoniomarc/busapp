package com.antonio.busapplication;

import android.content.Intent;
import android.content.res.AssetManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private boolean loggedIn = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void enterApp(View view) throws XmlPullParserException {
         List<User> users = new ArrayList<User>();
         List<Bus> buses = new ArrayList<Bus>();
        boolean firstUser = true;

        EditText nameView = (EditText) findViewById(R.id.editTextName);
        EditText passView = (EditText) findViewById(R.id.editTextPassword);
        try {
            users = User.readXmlFile(getApplicationContext(), true);
            buses = Bus.readXmlFile(getApplicationContext());
        } catch(IOException e) {
            e.printStackTrace();
        }

        try {
            User.writeXml(getApplicationContext(), users);
        } catch(IOException e) {
            e.printStackTrace();
        }

        String username = nameView.getText().toString();
        String password = passView.getText().toString();

        for(Bus b : buses) {
            AppRegistry.addBus(b);
        }

        for(User u : users) {
            AppRegistry.addUser(u);

            if (u.getUsername().equals(username) && u.getPassword().equals(password)) {
                AppRegistry.setCurrentUser(u);
                loggedIn = true;
                if(!u.getAdminRights()) {
                    Intent intent = new Intent(MainActivity.this, GeneralActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(MainActivity.this, AdminActivity.class);
                    startActivity(intent);
                }
                break;
            }
        }

        Log.i("loggedIn value", String.valueOf(loggedIn));

        if(!loggedIn) {
            Log.i("Warning", "Account info incorrect");
            Toast.makeText(this.getApplicationContext(), "Login Error !", Toast.LENGTH_SHORT).show();
        }
    }
}
