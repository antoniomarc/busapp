package com.antonio.busapplication.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.antonio.busapplication.Model.AppRegistry;
import com.antonio.busapplication.Model.Bus;
import com.antonio.busapplication.Model.FragmentType;
import com.antonio.busapplication.R;
import com.antonio.busapplication.Model.Report;
import com.antonio.busapplication.Model.ReportFactory;
import com.antonio.busapplication.Model.Route;
import com.antonio.busapplication.Model.User;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class AdminActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FragmentType fragmentType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fragmentType = FragmentType.USER_INFO_FRAGMENT;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.add_user) {
            Intent intent = new Intent(this, UserInputActivity.class);
            startActivityForResult(intent, 10);
        } else if (id == R.id.update_user) {
            Intent intent = new Intent(this, UserInputActivity.class);
            startActivityForResult(intent, 11);
        } else if (id == R.id.remove_user) {
            Intent intent = new Intent(this, UserInputActivity.class);
            startActivityForResult(intent, 12);
        } else if (id == R.id.view_user) {
            Intent intent = new Intent(this, UserInputActivity.class);
            startActivityForResult(intent, 13);
        } else if (id == R.id.add_bus) {
            Intent intent = new Intent(this, BusInputActivity.class);
            startActivityForResult(intent, 14);
        } else if (id == R.id.update_bus) {
            Intent intent = new Intent(this, BusInputActivity.class);
            startActivityForResult(intent, 15);
        } else if (id == R.id.remove_bus) {
            Intent intent = new Intent(this, BusInputActivity.class);
            startActivityForResult(intent, 16);
        } else if (id == R.id.view_bus) {
            Intent intent = new Intent(this, BusInputActivity.class);
            startActivityForResult(intent, 17);
        } else if (id == R.id.generate_report) {
            ReportFactory reportFactory = new ReportFactory();
            Report report = reportFactory.getReport("csv");
            report.generate(getApplicationContext(), AppRegistry.getBuses());
            report = reportFactory.getReport("pdf");
            report.generate(getApplicationContext(), AppRegistry.getBuses());
            Toast.makeText(getApplicationContext(), "Operation succesful !", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        UserInfoFragment userInfo = null;
        BusInfoFragment busInfo = null;

        if(fragmentType == FragmentType.USER_INFO_FRAGMENT) {
            userInfo = (UserInfoFragment) getSupportFragmentManager().findFragmentById(R.id.user_listview_fragment);
            busInfo = new BusInfoFragment();
        } else if (fragmentType == FragmentType.BUS_INFO_FRAGMENT) {
            userInfo = new UserInfoFragment();
            busInfo = (BusInfoFragment) getSupportFragmentManager().findFragmentById(R.id.bus_listview_fragment);
        }

        FragmentManager fm = getSupportFragmentManager();


        if (requestCode == 10) {
            if (resultCode == UserInputActivity.RESULT_OK) {

                if (fragmentType == FragmentType.BUS_INFO_FRAGMENT) {
                    FragmentTransaction transaction = fm.beginTransaction();
                    transaction.replace(R.id.bus_listview_fragment, userInfo);
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                    fm.executePendingTransactions();
                }

                HashMap<String, String> userData = (HashMap<String, String>) data.getSerializableExtra("User data");
                try {
                    try {
                        List<User> users = User.readXmlFile(getApplicationContext(), false);
                        User newUser = new User(userData.get("username"), userData.get("password"), Boolean.parseBoolean(userData.get("admin rights")));


                        Log.i("From admin activity", newUser.getEmail());

                        userInfo.clearList();
                        userInfo.addUserToList(newUser);

                        users.add(newUser);
                        User.writeXml(getApplicationContext(), users);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    fragmentType = FragmentType.USER_INFO_FRAGMENT;
                } catch (XmlPullParserException ex) {
                    ex.printStackTrace();
                }
              }
            } else if (requestCode == 11) {
                    if(resultCode == UserInputActivity.RESULT_OK) {
                if(fragmentType == FragmentType.BUS_INFO_FRAGMENT) {
                    FragmentTransaction transaction = fm.beginTransaction();
                    transaction.replace(R.id.bus_listview_fragment, userInfo);
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                    fm.executePendingTransactions();
                }

                    try {
                        try {
                            boolean firstUser = true;
                            HashMap<String, String> userData = (HashMap<String, String>) data.getSerializableExtra("User data");
                            List<User> users = User.readXmlFile(getApplicationContext(), false);
                            for (User u : users) {
                                if (u.getEmail().equals(userData.get("username"))) {
                                    Log.i("User", u.getEmail());
                                    u.setPassword(userData.get("password"));
                                    u.setAdminRights(Boolean.parseBoolean(userData.get("admin rights")));
                                    User newUser = new User(userData.get("username"), userData.get("password"), Boolean.parseBoolean(userData.get("admin rights")));
                                    userInfo.clearList();
                                    userInfo.addUserToList(newUser);
                                }
                            }

                            User.writeXml(getApplicationContext(), users);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        fragmentType = FragmentType.USER_INFO_FRAGMENT;
                    } catch (XmlPullParserException ex) {
                        ex.printStackTrace();
                    }
                }
            } else if (requestCode == 12) {
                if (resultCode == UserInputActivity.RESULT_OK) {

                    if(fragmentType == FragmentType.BUS_INFO_FRAGMENT) {
                        FragmentTransaction transaction = fm.beginTransaction();
                        transaction.replace(R.id.bus_listview_fragment, userInfo);
                        transaction.addToBackStack(null);
                        transaction.commitAllowingStateLoss();
                        fm.executePendingTransactions();
                    }

                    try {
                        try {
                            boolean firstUser = true;
                            HashMap<String, String> userData = (HashMap<String, String>) data.getSerializableExtra("User data");
                            List<User> users = User.readXmlFile(getApplicationContext(), false);
                            User removedUser = null;
                            userInfo.clearList();
                            for (User u : users) {
                                if (u.getEmail().equals(userData.get("username"))) {
                                    //userInfo.addUserToList(u);
                                    removedUser = u;

                                }
                            }

                            users.remove(removedUser);
                            Toast.makeText(getApplicationContext(), "User removed !", Toast.LENGTH_SHORT).show();
                            User.writeXml(getApplicationContext(), users);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        fragmentType = FragmentType.USER_INFO_FRAGMENT;
                    } catch (XmlPullParserException ex) {
                        ex.printStackTrace();
                    }
                }
            } else if (requestCode == 13) {
                if (resultCode == UserInputActivity.RESULT_OK) {

                    if(fragmentType == FragmentType.BUS_INFO_FRAGMENT) {
                        FragmentTransaction transaction = fm.beginTransaction();
                        transaction.replace(R.id.bus_listview_fragment, userInfo);
                        transaction.addToBackStack(null);
                        transaction.commitAllowingStateLoss();
                        fm.executePendingTransactions();
                    }

                    try {
                        try {
                            HashMap<String, String> userData = (HashMap<String, String>) data.getSerializableExtra("User data");
                            List<User> users = User.readXmlFile(getApplicationContext(), false);

                            userInfo.clearList();
                            for (User u : users) {
                                if (u.getEmail().equals(userData.get("username"))) {
                                    userInfo.addUserToList(u);
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        fragmentType = FragmentType.USER_INFO_FRAGMENT;
                    } catch (XmlPullParserException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        if (requestCode == 14) {
            if (resultCode == UserInputActivity.RESULT_OK) {

                if(fragmentType == FragmentType.USER_INFO_FRAGMENT) {
                    FragmentTransaction transaction = fm.beginTransaction();
                    transaction.replace(R.id.user_listview_fragment, busInfo);
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                    fm.executePendingTransactions();
                }

                HashMap<String, String> busData = (HashMap<String, String>) data.getSerializableExtra("Bus data");
                try {
                    try {
                        List<Bus> buses = Bus.readXmlFile(getApplicationContext(), false);
                        Bus newBus = new Bus(busData.get("bus name"), busData.get("bus type"), new Route(Long.parseLong(busData.get("route id"))));


                        //Log.i("From admin activity", newUser.getEmail());

                        busInfo.clearList();
                        busInfo.addBusToList(newBus);

                        buses.add(newBus);
                        AppRegistry.addBus(newBus);
                        Bus.writeXml(getApplicationContext(), buses);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    fragmentType = FragmentType.BUS_INFO_FRAGMENT;
                } catch (XmlPullParserException ex) {
                    ex.printStackTrace();
                }
            }
        }
        if (requestCode == 15) {
            if (resultCode == UserInputActivity.RESULT_OK) {

                if(fragmentType == FragmentType.USER_INFO_FRAGMENT) {
                    FragmentTransaction transaction = fm.beginTransaction();
                    transaction.replace(R.id.user_listview_fragment, busInfo);
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                    fm.executePendingTransactions();
                }

                HashMap<String, String> busData = (HashMap<String, String>) data.getSerializableExtra("Bus data");
                try {
                    try {
                        List<Bus> buses = Bus.readXmlFile(getApplicationContext(), false);


                        //   Log.i("From admin activity", newUser.getEmail());
                        for (Bus b : buses) {
                            if (b.getName().equals(busData.get("bus name"))) {
                                b.setType(busData.get("bus type"));
                                b.setRoute(new Route(Long.parseLong(busData.get("route id"))));
                                Bus newBus = new Bus(busData.get("bus name"), busData.get("bus type"), new Route(Long.parseLong(busData.get("route id"))));
                                busInfo.clearList();
                                busInfo.addBusToList(newBus);
                            }

                            Bus.writeXml(getApplicationContext(), buses);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    fragmentType = FragmentType.BUS_INFO_FRAGMENT;
                } catch (XmlPullParserException ex) {
                    ex.printStackTrace();
                }
            }
        }
        if (requestCode == 16) {
            if (resultCode == UserInputActivity.RESULT_OK) {

                if(fragmentType == FragmentType.USER_INFO_FRAGMENT) {
                    FragmentTransaction transaction = fm.beginTransaction();
                    transaction.replace(R.id.user_listview_fragment, busInfo);
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                    fm.executePendingTransactions();
                }

                try {
                    try {
                        HashMap<String, String> busData = (HashMap<String, String>) data.getSerializableExtra("Bus data");
                        List<Bus> buses = Bus.readXmlFile(getApplicationContext(), false);



                        Bus removedBus = null;
                        busInfo.clearList();
                        for (Bus b : buses) {
                            if (b.getName().equals(busData.get("bus name"))) {
                                removedBus = b;

                            }
                        }

                        buses.remove(removedBus);
                        Toast.makeText(getApplicationContext(), "Bus removed !", Toast.LENGTH_SHORT).show();
                        Bus.writeXml(getApplicationContext(), buses);
                        //Log.i("From admin activity", newUser.getEmail());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    fragmentType = FragmentType.BUS_INFO_FRAGMENT;
                } catch (XmlPullParserException ex) {
                    ex.printStackTrace();
                }
            }
        }
        if (requestCode == 17) {
            if (resultCode == UserInputActivity.RESULT_OK) {

                if(fragmentType == FragmentType.USER_INFO_FRAGMENT) {
                    FragmentTransaction transaction = fm.beginTransaction();
                    transaction.replace(R.id.user_listview_fragment, busInfo);
                    transaction.addToBackStack(null);
                    transaction.commitAllowingStateLoss();
                    fm.executePendingTransactions();
                }

                try {
                    try {
                        HashMap<String, String> busData = (HashMap<String, String>) data.getSerializableExtra("Bus data");
                        List<Bus> buses = Bus.readXmlFile(getApplicationContext(), false);

                        busInfo.clearList();
                        for (Bus b : buses) {
                            if (b.getName().equals(busData.get("bus name"))) {
                                busInfo.addBusToList(b);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    fragmentType = FragmentType.BUS_INFO_FRAGMENT;
                } catch (XmlPullParserException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
