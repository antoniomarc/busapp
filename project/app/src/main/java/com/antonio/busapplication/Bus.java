package com.antonio.busapplication;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 4/7/2017.
 */

public class Bus {

    private Long id;
    private Route route;
    private String name;
    private String type;

    private final static String busesPath = "buses.xml";

    private final static String findBusStatement = "SELECT Bus_id, Bus_name, Route_id, Bus_type" +
            "FROM Buses" +
            "WHERE Bus_name = ?";

    public Bus(Long id, Route route, String name, String type) {
        this.id = id;
        this.route = route;
        this.name = name;
        this.type = type;
    }

    public Bus(Long id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type = type;

    }

    private void writeXml() throws IOException {
        XmlSerializer serializer = Xml.newSerializer();
        FileWriter fw = new FileWriter("buses.xml", true);
        //StringWriter sw = new StringWriter();

        try {
            serializer.setOutput(fw);
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "bus");
            serializer.startTag("", "name");
            serializer.text(getName());
            serializer.endTag("", "name");
            serializer.startTag("", "type");
            serializer.text(getType());
            serializer.endTag("", "type");
            serializer.startTag("", "route");
            for(Station s : getRoute().getRouteStations()) {
                serializer.startTag("", "station");
                serializer.text(s.getName());
                serializer.endTag("" , "station");
            }
            serializer.endTag("", "route");
            serializer.endTag("", "bus");
            serializer.endDocument();
        } catch(Exception e) {}

        fw.close();
    }

    public static List<Bus> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<Bus> entries = new ArrayList<Bus>();

        parser.require(XmlPullParser.START_TAG, null, "buses");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("bus")) {
                entries.add(readEntry(parser));
            } else {
                skip(parser);
            }
        }
        return entries;
    }


    public static Bus readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, null, "bus");
        String busName = null;
        String busType = null;
        Route route = null;
        Long busId = null;

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("bus_name")) {
                busName = readBusName(parser);
            } else if (name.equals("bus_type")) {
                busType = readBusType(parser);
            } else if (name.equals("route")) {
                route = readBusRoute(parser);
            } else if (name.equals("bus_id")) {
                busId = readBusID(parser);
            }
            else {
                skip(parser);
            }
        }

        return new Bus(busId, route, busName, busType);
    }

    public static String readBusName(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "bus_name");
        String busName = readText(parser);
        Log.i("User:", busName);
        parser.require(XmlPullParser.END_TAG, null, "bus_name");
        return busName;
    }

    public static String readBusType(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "bus_type");
        String busType = readText(parser);
        Log.i("User:", busType);
        parser.require(XmlPullParser.END_TAG, null, "bus_type");
        return busType;
    }

    public static Long readBusID(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "bus_id");
        Long busId = Long.parseLong(readText(parser));
        //Log.i("User:", busId);
        parser.require(XmlPullParser.END_TAG, null, "bus_id");
        return busId;
    }

    public static Route readBusRoute(XmlPullParser parser) throws IOException, XmlPullParserException {
        List<Station> entries = new ArrayList<Station>();

        parser.require(XmlPullParser.START_TAG, null, "route");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("station")) {
                entries.add(readStation(parser));
            } else {
                skip(parser);
            }
        }
        return new Route(entries);
    }

    public static Station readStation(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "station");
        String stationName = null;

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("station_name")) {
                stationName = readStationName(parser);
            } else {
                skip(parser);
            }
        }

        return new Station(stationName);
    }

    public static String readStationName(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "station_name");
        String stationName = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "station_name");
        return stationName;
    }

    public static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }


    public static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }


    public static List<Bus> readXmlFile(Context context) throws XmlPullParserException, IOException {
        XmlPullParserFactory xppf = XmlPullParserFactory.newInstance();
        xppf.setNamespaceAware(true);
        XmlPullParser parser = xppf.newPullParser();


        //File myXML = new File(path);
        // FileInputStream fis = new FileInputStream();
        InputStream is = null;

        try {
            AssetManager am = context.getAssets();
            is = am.open(busesPath);

            parser.setInput(new InputStreamReader(is));
            parser.nextTag();
            return readFeed(parser);

        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch(IOException e) {}
        }

        return readFeed(parser);
    }


    @Override
    public String toString() {
        return this.name + this.route;

    }



    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
