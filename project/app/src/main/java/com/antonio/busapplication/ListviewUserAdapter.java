package com.antonio.busapplication;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 4/17/2017.
 */

class ListviewUserAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private static List<User> listUsers;

    public ListviewUserAdapter(Context userInfoFragment, List<User> results) {
        listUsers = results;
        inflater = LayoutInflater.from(userInfoFragment);
    }


    @Override
    public int getCount() {
        return listUsers.size();
    }

    @Override
    public User getItem(int i) {
        return listUsers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            view = inflater.inflate(R.layout.listview, null);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Log.i("From adapter:", listUsers.get(i).getUsername());

        viewHolder.getName().setText(listUsers.get(i).getUsername());
        viewHolder.getPassword().setText(listUsers.get(i).getPassword());
        viewHolder.getAdminRights().setText(listUsers.get(i).getAdminRights().toString());
        return view;
    }
}
