package com.antonio.busapplication;

import android.content.Context;
import android.content.res.AssetManager;
import android.location.Location;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 4/7/2017.
 */

public class User {

    private final static String usersPath = "users.xml";
    private static Boolean firstTimeWriting = true;

    private String username;
    private String password;
    private Boolean adminRights;

    private Bus bus;
    private ArrayList<Bus> busesToShow;
    private Location location;


    public User(String username, String password, Boolean adminRights) {
        this.username = username;
        this.password = password;
        this.adminRights = adminRights;
    }

    public User(Bus bus, ArrayList<Bus> buses) {
        this.bus = bus;
        busesToShow = new ArrayList<Bus>(buses);

    }

    public User(Location location) {
        this.location = new Location(location);
    }


    public ArrayList<Bus> searchBus(String name) {
        ArrayList<Bus> buses = new ArrayList<>();

        for(Bus b : busesToShow) {
            if(b.getName().equals(name))
                buses.add(b);
        }

        return buses;
    }

    public static void writeXml(Context context, List<User> users) throws IOException {
        XmlSerializer serializer = Xml.newSerializer();
        FileOutputStream fileos = context.getApplicationContext().openFileOutput("users_file.xml", Context.MODE_PRIVATE);

        StringWriter sw = new StringWriter();

        try {
            serializer.setOutput(sw);
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "users");
            for(User u : users) {
                serializer.startTag("", "user");
                serializer.startTag("", "username");
                serializer.text(u.getUsername());
                serializer.endTag("", "username");
                serializer.startTag("", "password");
                serializer.text(u.getPassword());
                serializer.endTag("", "password");
                serializer.startTag("", "admin_rights");
                serializer.text(u.getAdminRights().toString());
                serializer.endTag("", "admin_rights");
                serializer.endTag("", "user");
            }
            serializer.endTag("", "users");
            serializer.endDocument();
            String dataWrite = sw.toString();
            fileos.write(dataWrite.getBytes());
        } catch(Exception e) {
            e.printStackTrace();
        }

        fileos.close();
    }


    public static List<User> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        List entries = new ArrayList<User>();

        parser.require(XmlPullParser.START_TAG, null, "users");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("user")) {
                entries.add(readEntry(parser));
            } else {
                skip(parser);
            }
        }
        return entries;
    }


    public static User readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, null, "user");
        String username = null;
        String password = null;
        Boolean adminRights = false;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("username")) {
                username = readUsername(parser);
            } else if (name.equals("password")) {
                password = readPassword(parser);
            } else if (name.equals("admin_rights")) {
                adminRights = readAdminRights(parser);
            } else {
                skip(parser);
            }
        }

        return new User(username, password, adminRights);
    }

    private static String readUsername(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "username");
        String username = readText(parser);
        Log.i("User:", username);
        parser.require(XmlPullParser.END_TAG, null, "username");
        return username;
    }

    private static String readPassword(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "password");
        String password = readText(parser);
        Log.i("Password", password);
        parser.require(XmlPullParser.END_TAG, null, "password");
        return password;
    }
    
    private static Boolean readAdminRights(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "admin_rights");
        Boolean adminRights = Boolean.parseBoolean(readText(parser));
        parser.require(XmlPullParser.END_TAG, null, "admin_rights");
        return adminRights;
    }


    public static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }


    public static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }


    public static List<User> readXmlFile(Context context, boolean fromAssets) throws XmlPullParserException, IOException {
        XmlPullParserFactory xppf = XmlPullParserFactory.newInstance();
        xppf.setNamespaceAware(true);
        XmlPullParser parser = xppf.newPullParser();


        //File myXML = new File(path);
        // FileInputStream fis = new FileInputStream();
        InputStream is = null;
        FileInputStream fileis = null;
        try {
            if(!fromAssets) {
                fileis = context.getApplicationContext().openFileInput("users_file.xml");
                parser.setInput(new InputStreamReader(fileis));
            } else {
                AssetManager as = context.getAssets();
                is = as.open("users.xml");
                parser.setInput(new InputStreamReader(is));
            }

            parser.nextTag();
            return readFeed(parser);

        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(!fromAssets)
                    fileis.close();
                else
                is.close();
            } catch(IOException e) {
                e.printStackTrace();
            }
        }

        return readFeed(parser);
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getAdminRights() {
        return adminRights;
    }

    public void setAdminRights(Boolean adminRights) {
        this.adminRights = adminRights;
    }
}
