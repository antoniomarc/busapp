package com.antonio.busapplication;

/**
 * Created by Antonio on 4/17/2017.
 */

public interface Report {

   void generate(Bus bus);

}
