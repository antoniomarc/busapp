package com.antonio.busapplication;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 4/7/2017.
 */

public class Route {
    private List<LatLng> polyLine;
    private List<Station> routeStations;

    public Route(List<Station> routeStations) {
        this.routeStations = new ArrayList<Station>(routeStations);
    }

    public List<Station> getRouteStations() {
        return routeStations;
    }

    public void setRouteStations(List<Station> routeStations) {
        this.routeStations = routeStations;
    }
}
