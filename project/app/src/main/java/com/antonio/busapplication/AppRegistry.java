package com.antonio.busapplication;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by Antonio on 4/12/2017.
 */

public class AppRegistry {

    public static List<Bus> buses = new ArrayList<Bus>();
    public static List<User> users = new ArrayList<User>();
    public static TreeMap<Bus, Integer> userNo = new TreeMap<Bus, Integer>();
    public static User currentUser;

    public static void addBus(Bus b) {
        buses.add(b);
    }

    public static void addUser(User u) {
        users.add(u);
    }

    public static void setCurrentUser(User u) {
        currentUser = u;
    }

    public static List<Bus> getBusesByName(String busName) {
        List<Bus> busesToShow = new ArrayList<Bus>();

        for (Bus b : buses) {
            if(b.getName().equals(busName))
                busesToShow.add(b);
        }

        return busesToShow;
    }
}
