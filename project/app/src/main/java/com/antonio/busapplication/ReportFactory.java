package com.antonio.busapplication;

/**
 * Created by Antonio on 4/17/2017.
 */

public class ReportFactory {

    public Report getReport(String type) {
        if(type.equals("csv")) {
            return new CSVReport();
        } else if (type.equals("pdf")) {
            return new PDFReport();
        } else
            return null;
    }
}
