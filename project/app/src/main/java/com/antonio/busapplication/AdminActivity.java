package com.antonio.busapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdminActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.add_user) {
            Intent intent = new Intent(this, UserInputActivity.class);
            startActivityForResult(intent, 10);
        } else if (id == R.id.update_user) {
            Intent intent = new Intent(this, UserInputActivity.class);
            startActivityForResult(intent, 11);
        } else if (id == R.id.remove_user) {
            Intent intent = new Intent(this, UserInputActivity.class);
            startActivityForResult(intent, 12);
        } else if (id == R.id.view_user) {
            Intent intent = new Intent(this, UserInputActivity.class);
            startActivityForResult(intent, 13);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        UserInfoFragment userInfo = (UserInfoFragment) getSupportFragmentManager().findFragmentById(R.id.list_view_fragment);

        if (requestCode == 10) {
            if (resultCode == UserInputActivity.RESULT_OK) {
                HashMap<String, String> userData = (HashMap<String, String>) data.getSerializableExtra("User data");
                try {
                    try {
                        List<User> users = User.readXmlFile(getApplicationContext(), false);
                        User newUser = new User(userData.get("username"), userData.get("password"), Boolean.parseBoolean(userData.get("admin rights")));


                        Log.i("From admin activity", newUser.getUsername());

                        userInfo.clearList();
                        userInfo.addUserToList(newUser);

                        users.add(newUser);
                        User.writeXml(getApplicationContext(), users);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (XmlPullParserException ex) {
                    ex.printStackTrace();
                }
            }
        } else if (requestCode == 11) {
            if (resultCode == UserInputActivity.RESULT_OK) {
                try {
                    try {
                        boolean firstUser = true;
                        HashMap<String, String> userData = (HashMap<String, String>) data.getSerializableExtra("User data");
                        List<User> users = User.readXmlFile(getApplicationContext(), false);
                        for (User u : users) {
                            if (u.getUsername().equals(userData.get("username"))) {
                                u.setPassword(userData.get("password"));
                                u.setAdminRights(Boolean.parseBoolean(userData.get("admin rights")));
                                User newUser = new User(userData.get("username"), userData.get("password"), Boolean.parseBoolean(userData.get("admin rights")));
                                userInfo.clearList();
                                userInfo.addUserToList(newUser);
                            }
                        }

                        User.writeXml(getApplicationContext(), users);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (XmlPullParserException ex) {
                    ex.printStackTrace();
                }
            }
        } else if (requestCode == 12) {
            if (resultCode == UserInputActivity.RESULT_OK) {
                try {
                    try {
                        boolean firstUser = true;
                        HashMap<String, String> userData = (HashMap<String, String>) data.getSerializableExtra("User data");
                        List<User> users = User.readXmlFile(getApplicationContext(), false);
                        User removedUser = null;
                        userInfo.clearList();
                        for (User u : users) {
                            if (u.getUsername().equals(userData.get("username"))) {
                                userInfo.addUserToList(u);
                                removedUser = u;

                            }
                        }

                        users.remove(removedUser);
                        User.writeXml(getApplicationContext(), users);
                    } catch (IOException e) {

                    }
                } catch (XmlPullParserException ex) {

                }
            }
        } else if (requestCode == 13) {
            if (resultCode == UserInputActivity.RESULT_OK) {
                try {
                    try {
                        HashMap<String, String> userData = (HashMap<String, String>) data.getSerializableExtra("User data");
                        List<User> users = User.readXmlFile(getApplicationContext(), false);

                        userInfo.clearList();
                        for (User u : users) {
                            if (u.getUsername().equals(userData.get("username"))) {
                                userInfo.addUserToList(u);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (XmlPullParserException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
