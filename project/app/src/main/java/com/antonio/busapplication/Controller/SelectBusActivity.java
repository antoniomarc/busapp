package com.antonio.busapplication.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.antonio.busapplication.R;

import java.util.List;

public class SelectBusActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_bus);
    }

    public void findBus(View view) {
        EditText busName = (EditText) findViewById(R.id.findBusEditText);

        Intent resultData = new Intent();

        resultData.putExtra("Bus", busName.getText().toString());
        setResult(this.RESULT_OK, resultData);

        finish();

    }


}
