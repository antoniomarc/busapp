package com.antonio.busapplication;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Antonio on 4/17/2017.
 */

public class ViewHolder extends RecyclerView.ViewHolder{
    private TextView name;
    private TextView password;
    private TextView adminRights;

    public ViewHolder(View itemView) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.username);
        password = (TextView) itemView.findViewById(R.id.password);
        adminRights = (TextView) itemView.findViewById(R.id.admin_rights);
    }

    public TextView getName() {
        return name;
    }

    public TextView getPassword() {
        return password;
    }

    public TextView getAdminRights() {
        return adminRights;
    }
}
