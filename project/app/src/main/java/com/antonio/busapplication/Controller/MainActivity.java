package com.antonio.busapplication.Controller;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.antonio.busapplication.Model.AppRegistry;
import com.antonio.busapplication.Model.Bus;
import com.antonio.busapplication.Model.LocationService;
import com.antonio.busapplication.R;
import com.antonio.busapplication.Model.Route;
import com.antonio.busapplication.Model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private boolean loggedIn = false;
    private FirebaseAuth mAuth;
    private boolean fromAssets = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();
    }

    public void enterWithoutAccount(View view) {

        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                           // Log.d(TAG, "signInAnonymously:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            //AppRegistry.setCurrentUser(new User(user.getDisplayName()));
                            Intent intent = new Intent(MainActivity.this, GeneralActivity.class);
                            startActivity(intent);
                        } else {
                            // If sign in fails, display a message to the user.
                           // Log.w(TAG, "signInAnonymously:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                           // updateUI(null);
                        }
                    }
                });
    }

    public void enterApp(View view) throws XmlPullParserException {
       /*  List<User> users = new ArrayList<User>();
         List<Bus> buses = new ArrayList<Bus>();
        boolean firstUser = true;

        EditText nameView = (EditText) findViewById(R.id.editTextName);
        EditText passView = (EditText) findViewById(R.id.editTextPassword);
        try {
            if(fromAssets) {
                users = User.readXmlFile(getApplicationContext(), fromAssets);
                buses = Bus.readXmlFile(getApplicationContext(), fromAssets);
            } else {
                users.add(new User("admin", "admin", true));
                users.add(new User("antonio", "pass123", false));
                buses.add(new Bus("35", "local", new Route((long) 1)));
            }
        } catch(IOException e) {
            e.printStackTrace();
        }

        try {
            User.writeXml(getApplicationContext(), users);
            Bus.writeXml(getApplicationContext(), buses);
        } catch(IOException e) {
            e.printStackTrace();
        }

        String username = nameView.getText().toString();
        String password = passView.getText().toString();

        for(Bus b : buses) {
            AppRegistry.addBus(b);
        }
*/
        EditText nameView = (EditText) findViewById(R.id.editTextName);
        EditText passView = (EditText) findViewById(R.id.editTextPassword);

        mAuth.signInWithEmailAndPassword(nameView.getText().toString(), passView.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                          //  Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            AppRegistry.setCurrentUser(new User(user.getEmail()));
                            Intent intent = new Intent(MainActivity.this, GeneralActivity.class);
                            startActivity(intent);
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });



       /* for(User u : users) {
            AppRegistry.addUser(u);

            if (u.getEmail().equals(username) && u.getPassword().equals(password)) {
                AppRegistry.setCurrentUser(u);
                loggedIn = true;
                if(!u.getAdminRights()) {
                    Intent intent = new Intent(MainActivity.this, GeneralActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(MainActivity.this, AdminActivity.class);
                    startActivity(intent);
                }
                break;
            }
        }

        Log.i("loggedIn value", String.valueOf(loggedIn));

        if(!loggedIn) {
            Log.i("Warning", "Account info incorrect");
            Toast.makeText(this.getApplicationContext(), "Login Error !", Toast.LENGTH_SHORT).show();
        }*/
    }

    public void registerAccount(View view) {
        EditText nameView = (EditText) findViewById(R.id.editTextName);
        EditText passView = (EditText) findViewById(R.id.editTextPassword);

        mAuth.createUserWithEmailAndPassword(nameView.getText().toString(), passView.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            //Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(MainActivity.this, "User created !", Toast.LENGTH_SHORT).show();
                        } else {
                            // If sign in fails, display a message to the user.
                           // Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                           // updateUI(null);
                        }

                        // ...
                    }
                });

    }

    public void linkAccount(View view) {

        EditText nameView = (EditText) findViewById(R.id.editTextName);
        EditText passView = (EditText) findViewById(R.id.editTextPassword);
        // Make sure form is valid
        if (!validateLinkForm()) {
            return;
        }

        // Get email and password from form
        String email = nameView.getText().toString();
        String password = passView.getText().toString();

        // Create EmailAuthCredential with email and password
        AuthCredential credential = EmailAuthProvider.getCredential(email, password);


        // [START link_credential]
        mAuth.getCurrentUser().linkWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                           // Log.d(TAG, "linkWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();
                            AppRegistry.setCurrentUser(new User(user.getEmail()));
                            Intent intent = new Intent(MainActivity.this, GeneralActivity.class);
                            startActivity(intent);
                        } else {
                           // Log.w(TAG, "linkWithCredential:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });
        // [END link_credential]
    }

    private boolean validateLinkForm() {
        EditText nameView = (EditText) findViewById(R.id.editTextName);
        EditText passView = (EditText) findViewById(R.id.editTextPassword);
        boolean valid = true;

        String email = nameView.getText().toString();
        if (TextUtils.isEmpty(email)) {
            nameView.setError("Required.");
            valid = false;
        } else {
           nameView.setError(null);
        }

        String password = passView.getText().toString();
        if (TextUtils.isEmpty(password)) {
            passView.setError("Required.");
            valid = false;
        } else {
            passView.setError(null);
        }

        return valid;
    }

    @Override
    public void onPause() {
        super.onPause();

        if(isFinishing()) {
            Intent intent = new Intent(this, LocationService.class);
            stopService(intent);
        }

    }


    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public boolean isFromAssets() {
        return fromAssets;
    }

    public void setFromAssets(boolean fromAssets) {
        this.fromAssets = fromAssets;
    }
}
