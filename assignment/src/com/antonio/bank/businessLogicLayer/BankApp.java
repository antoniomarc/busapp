package com.antonio.bank.businessLogicLayer;

import com.antonio.bank.controllerLayer.BankController;
import com.antonio.bank.dataSourceLayer.*;
import com.antonio.bank.presentationLayer.BankGUI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Antonio on 3/20/2017.
 */
public class BankApp {

    private final static String createAccountTableStmt = "CREATE TABLE IF NOT EXISTS Account (" +
            "Account_ID INT PRIMARY KEY NOT NULL," +
            "    AccNo VARCHAR(45) NULL," +
            "    Type VARCHAR(45) NULL," +
            "    Money  VARCHAR(45) NULL," +
            "    Date  VARCHAR(45) NULL," +
            "    Personal_Code VARCHAR(45)," +
            "    FOREIGN KEY (Personal_Code) " +
            "       REFERENCES Client (Personal_Code)" +
            ");";

    private final static String createReportTableStmt = "CREATE TABLE IF NOT EXISTS Report (" +
            "Report_ID INT PRIMARY KEY NOT NULL," +
            "Date Text NULL," +
            "Activity VARCHAR NULL," +
            "Personal_Code VARCHAR," +
            "FOREIGN KEY (Personal_Code) " +
            "REFERENCES Employee (Personal_Code)" +
            ");";


    private final static String createClientTableStmt = "CREATE TABLE IF NOT EXISTS Client (" +
            "Client_ID INT PRIMARY KEY NOT NULL," +
            "    Name VARCHAR(45) NULL," +
            "    Personal_Code VARCHAR(45) NULL," +
            "    Email  VARCHAR(45) NULL," +
            "    Address  VARCHAR(45) NULL" +
            ");";

    private final static String createEmployeeTableStmt = "CREATE TABLE IF NOT EXISTS Employee (" +
            "Employee_ID INT PRIMARY KEY NOT NULL," +
            "    Name VARCHAR(45) NULL," +
            "    Personal_Code VARCHAR(45) NULL," +
            "    Email  VARCHAR(45) NULL," +
            "    Address  VARCHAR(45) NULL," +
            "    Password VARCHAR(45) NULL," +
            "    AdminRights INT NULL" +
            ");";

    private final static String createBillTableStmt = "CREATE TABLE IF NOT EXISTS Bill (" +
            "Bill_ID INT PRIMARY KEY NOT NULL," +
            "    Amount  REAL NULL," +
            "    Personal_Code VARCHAR," +
            "    FOREIGN KEY (Personal_Code) " +
            "       REFERENCES Client (Personal_Code) " +
            ");";

    private final static String populateDBString = "INSERT INTO Employee VALUES " +
            "('1', 'Marc Antonio', '1924762692431', 'antomarc19@yahoo.com', 'Nicolae Balcescu nr. 17', 'pass123', '1')," +
            "('2', 'Popescu Ion', '2428348952983', 'popescuion@gmail.com', '21 decembrie nr 23', 'pass1234', '0');";

    private final static String populateBillString = "INSERT INTO Bill VALUES " +
            "('1', '20.0', '112')," +
            "('2', '10.0', '112');";

    private BankGUI bankGUI;
    private BankController bankController;
    private BankRegistry bankRegistry;

    public BankApp() {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement stmt = null;

        try {
            stmt = c.prepareStatement(createClientTableStmt);
            stmt.execute();
            stmt = c.prepareStatement(createEmployeeTableStmt);
            stmt.execute();
            stmt = c.prepareStatement(createAccountTableStmt);
            stmt.execute();
            stmt = c.prepareStatement(createBillTableStmt);
            stmt.execute();
            stmt = c.prepareStatement(createReportTableStmt);
            stmt.execute();
          //  stmt = c.prepareStatement(populateDBString);
          //  stmt.execute();


            Client.populateDb();
            Employee.populateDb();
            Account.populateDb();
            Bill.populateDb();
            Report.populateDb();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
//                c.commit();
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        //BankRegistry.registerEmployee(new Employee(Long.parseLong("1"), "Marc Antonio", "1924762692431", "antomarc19@yahoo.com",
        //        "Nicolae Balcescu nr. 17", "pass123", true));
       // BankRegistry.registerEmployee(new Employee(Long.parseLong("2"), "Popescu Ion", "2428348952983", "popescuion@gmail.com",
        //        "21 Decembrie nr 23", "pass1234", false));

        bankGUI = new BankGUI();
        bankController = new BankController(bankGUI);
    }

    public static void main(String args[]) {
        new BankApp();
    }


}
