package com.antonio.bank.businessLogicLayer;


import com.antonio.bank.dataSourceLayer.*;

import java.util.*;

/**
 * Created by Antonio on 3/18/2017.
 */
public class BankRegistry {
    private static Map<Client, List<Account>> bankAccounts = new HashMap<Client, List<Account>>();
    private static Map<Client, List<Bill>> clientBills = new HashMap<Client, List<Bill>>();
    private static List<Employee> employees = new ArrayList<Employee>();
    private static Map<String, List<Report>> employeeActivity = new HashMap<String,List<Report>>();

    public static void registerAccount(String clientID, Account reference) {
        for(Map.Entry<Client, List<Account>> bankEntry : bankAccounts.entrySet())
            if(bankEntry.getKey().getPersonalCode().equals(clientID)) {
                bankEntry.getValue().add(reference);
            }
    }

    public static void registerReport(String employeeID, Report reference) {
        for(Map.Entry<String, List<Report>> bankEntry : employeeActivity.entrySet())
            if(bankEntry.getKey().equals(employeeID)) {
                bankEntry.getValue().add(reference);
            }
    }

    public static void registerBill (String clientID, Bill reference) {
        for(Map.Entry<Client, List<Bill>> bankEntry : clientBills.entrySet())
            if(bankEntry.getKey().getPersonalCode().equals(clientID)) {
                bankEntry.getValue().add(reference);
            }
    }

    public static void registerClient(Client reference) {
        bankAccounts.put(reference, new ArrayList<Account>());
        clientBills.put(reference, new ArrayList<Bill>());
    }

    public static void registerEmployee(Employee reference) {
        employees.add(reference);
        employeeActivity.put(reference.getPersonalCode(), new ArrayList<Report>());
    }

    public static Employee getEmployee(String employeeID) {
        for (Employee e: employees) {
            if (e.getPersonalCode().equals(employeeID))
                return e;
        }
        return null;
    }

    public static Report getReport(String reportID) {
        for(Map.Entry<String, List<Report>> report : employeeActivity.entrySet()) {
            if(!report.getValue().isEmpty())
            for(Report r : report.getValue())
                    if(r.getReportID().equals(Long.parseLong(reportID)))
                return r;
        }
        return null;
    }

    public static Employee getEmployeeByUser(String employeeUser) {
        for (Employee e: employees) {
            if (e.getName().equals(employeeUser)) {
                System.out.println(e.toString());
                return e;
            }
        }
        return null;
    }


    public static Client getClient(String personalCode) {
        for (Map.Entry<Client, List<Account>> bankEntry : bankAccounts.entrySet())
            if (bankEntry.getKey().getPersonalCode().equals(personalCode))
                return bankEntry.getKey();
        return null;
    }

    public static Account getAccount(String accountNo) {
        for (Map.Entry<Client, List<Account>> bankEntry : bankAccounts.entrySet()) {
            if(!bankEntry.getValue().isEmpty())
            for (Account a : bankEntry.getValue())
                if (a.getAccNo().equals(accountNo))
                    return a;
        }
        return null;
    }

    public static Bill getBill(String billID) {
        for (Map.Entry<Client, List<Bill>> bankEntry : clientBills.entrySet()) {
            if (!bankEntry.getValue().isEmpty())
                for (Bill b : bankEntry.getValue())
                    if (b.getBillID().equals(billID)) {
                        System.out.println(b.getAmount());
                        return b;
                    }
        }
        return null;
    }

    public static void removeAccount(String clientID, Account reference) {
        for(Map.Entry<Client, List<Account>> bankEntry : bankAccounts.entrySet())
            if(bankEntry.getKey().getPersonalCode().equals(clientID)) {
                bankEntry.getValue().remove(reference);
            }
    }

    public static void removeBill(String clientID, Bill reference) {
        for(Map.Entry<Client, List<Bill>> bankEntry :clientBills.entrySet())
            if(bankEntry.getKey().getPersonalCode().equals(clientID)) {
                bankEntry.getValue().remove(reference);
            }
    }

    public static void removeEmployee(Employee reference) {
        employees.remove(reference);
    }


}
