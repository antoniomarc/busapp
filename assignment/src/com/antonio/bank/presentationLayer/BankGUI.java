package com.antonio.bank.presentationLayer;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

/**
 * Created by Antonio on 3/25/2017.
 */
public class BankGUI extends JFrame {
    public BankGUI() {
        initComponents();
    }

    private void enterMouseClicked(MouseEvent e) {
        panel1.setVisible(false);
        panel2.setVisible(true);
    }

    private void initComponents() {
        windowLayout = new CardLayout();
        panel3 = new JPanel();
        label8 = new JLabel();
        label10 = new JLabel();
        label9 = new JLabel();
        createEmployeeBtn = new JButton();
        scrollPane2 = new JScrollPane();
        textArea2 = new JTextArea();
        updateEmployeeBtn = new JButton();
        viewEmployeeBtn = new JButton();
        deleteEmployeeBtn = new JButton();
        generateReportBtn = new JButton();
        panel2 = new JPanel();
        label4 = new JLabel();
        label5 = new JLabel();
        label6 = new JLabel();
        label7 = new JLabel();
        addClientBtn = new JButton();
        addAccountBtn = new JButton();
        updateClientBtn = new JButton();
        updateAccountBtn = new JButton();
        scrollPane1 = new JScrollPane();
        textArea1 = new JTextArea();
        viewClientBtn = new JButton();
        viewAccountBtn = new JButton();
        deleteAccountBtn = new JButton();
        transferAccountBtn = new JButton();
        panel1 = new JPanel();
        label1 = new JLabel();
        label2 = new JLabel();
        textField1 = new JTextField();
        label3 = new JLabel();
        passwordField1 = new JPasswordField();
        enterAppBtn = new JButton();
        dialog1 = new JDialog();
        label11 = new JLabel();
        label14 = new JLabel();
        textField2 = new JTextField();
        passwordField2 = new JPasswordField();
        label12 = new JLabel();
        label15 = new JLabel();
        textField3 = new JTextField();
        textField6 = new JTextField();
        label13 = new JLabel();
        label16 = new JLabel();
        textField4 = new JTextField();
        textField7 = new JTextField();
        processEmployeeBtn = new JButton();
        dialog2 = new JDialog();
        label17 = new JLabel();
        label20 = new JLabel();
        textField8 = new JTextField();
        textField11 = new JTextField();
        label18 = new JLabel();
        label19 = new JLabel();
        textField9 = new JTextField();
        textField10 = new JTextField();
        processClientBtn = new JButton();
        dialog3 = new JDialog();
        label21 = new JLabel();
        textField12 = new JTextField();
        label22 = new JLabel();
        textField13 = new JTextField();
        label23 = new JLabel();
        textField14 = new JTextField();
        processAccountBtn = new JButton();
        dialog4 = new JDialog();
        label24 = new JLabel();
        textField15 = new JTextField();
        label25 = new JLabel();
        textField16 = new JTextField();
        label26 = new JLabel();
        label27 = new JLabel();
        textField5 = new JTextField();
        textField17 = new JTextField();
        transferAccConfirmBtn = new JButton();
        scrollPane3 = new JScrollPane();
        textArea3 = new JTextArea();
        dialog5 = new JDialog();
        label28 = new JLabel();
        label29 = new JLabel();
        label30 = new JLabel();
        textField18 = new JTextField();
        textField19 = new JTextField();
        textField20 = new JTextField();
        button19 = new JButton();
        label31 = new JLabel();
        textField21 = new JTextField();
        dialog6 = new JDialog();
        label32 = new JLabel();
        textField22 = new JTextField();
        label33 = new JLabel();
        textField23 = new JTextField();
        button20 = new JButton();
        button21 = new JButton();
        label34 = new JLabel();
        textField24 = new JTextField();

        //======== this ========
        setVisible(true);
        Container contentPane = getContentPane();
        contentPane.setLayout(windowLayout);



        //======== panel3 ========
        {

            // JFormDesigner evaluation mark
            panel3.setBorder(new javax.swing.border.CompoundBorder(
                    new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                            "", javax.swing.border.TitledBorder.CENTER,
                            javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                            java.awt.Color.red), panel3.getBorder())); panel3.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

            panel3.setLayout(new MigLayout(
                    "hidemode 3",
                    // columns
                    "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]",
                    // rows
                    "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]0" +
                            "[]0" +
                            "[]0" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]"));

            //---- label8 ----
            label8.setText("BankApp");
            panel3.add(label8, "cell 2 0");

            //---- label10 ----
            label10.setText("Info display:");
            panel3.add(label10, "cell 7 1");

            //---- label9 ----
            label9.setText("Employee");
            panel3.add(label9, "cell 3 2");

            //---- createEmployeeBtn ----
            createEmployeeBtn.setText("Create");
            panel3.add(createEmployeeBtn, "cell 3 4");

            //======== scrollPane2 ========
            {
                scrollPane2.setViewportView(textArea2);
            }
            panel3.add(scrollPane2, "cell 6 2 5 9,dock center");

            //---- updateEmployeeBtn ----
            updateEmployeeBtn.setText("Update");
            panel3.add(updateEmployeeBtn, "cell 3 7,alignx trailing,growx 0");

            //---- viewEmployeeBtn ----
            viewEmployeeBtn.setText("View");
            panel3.add(viewEmployeeBtn, "cell 3 10");

            //---- deleteEmployeeBtn ----
            deleteEmployeeBtn.setText("Delete");
            panel3.add(deleteEmployeeBtn, "cell 3 12");

            //---- generateReportBtn ----
            generateReportBtn.setText("Generate report");
            panel3.add(generateReportBtn, "cell 7 12");
        }
        contentPane.add(panel3, "card3");

        //======== panel2 ========
        {
            panel2.setLayout(new MigLayout(
                    "hidemode 3",
                    // columns
                    "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]",
                    // rows
                    "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]"));

            //---- label4 ----
            label4.setText("BankApp");
            panel2.add(label4, "cell 2 0");

            //---- label5 ----
            label5.setText("Client");
            panel2.add(label5, "cell 1 1");

            //---- label6 ----
            label6.setText("Account");
            panel2.add(label6, "cell 5 1");

            //---- label7 ----
            label7.setText("Info display:");
            panel2.add(label7, "cell 10 2");

            //---- addClientBtn ----
            addClientBtn.setText("Add ");
            panel2.add(addClientBtn, "cell 1 3");

            //---- addAccountBtn ----
            addAccountBtn.setText("Create");
            panel2.add(addAccountBtn, "cell 5 3");

            //---- updateClientBtn ----
            updateClientBtn.setText("Update");
            panel2.add(updateClientBtn, "cell 1 4");

            //---- button21 ----
            button21.setText("Pay Bill");
            panel2.add(button21, "cell 1 6");

            //---- updateAccountBtn ----
            updateAccountBtn.setText("Update");
            panel2.add(updateAccountBtn, "cell 5 4");

            //======== scrollPane1 ========
            {
                scrollPane1.setViewportView(textArea1);
            }
            panel2.add(scrollPane1, "cell 9 3 3 5,width 100:100:110,height 120:120:150");

            //---- viewClientBtn ----
            viewClientBtn.setText("View");
            panel2.add(viewClientBtn, "cell 1 5");

            //---- viewAccountBtn ----
            viewAccountBtn.setText("View");
            panel2.add(viewAccountBtn, "cell 5 5");

            //---- deleteAccountBtn ----
            deleteAccountBtn.setText("Delete");
            panel2.add(deleteAccountBtn, "cell 5 6");

            //---- transferAccountBtn ----
            transferAccountBtn.setText("Transfer ");
            panel2.add(transferAccountBtn, "cell 5 7");
        }
        contentPane.add(panel2, "card2");

        //======== panel1 ========
        {
            panel1.setLayout(new MigLayout(
                    "hidemode 3",
                    // columns
                    "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]",
                    // rows
                    "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]"));

            //---- label1 ----
            label1.setText("BankApp");
            panel1.add(label1, "cell 2 0");

            //---- label2 ----
            label2.setText("Enter username:");
            panel1.add(label2, "cell 3 2");
            panel1.add(textField1, "cell 3 3");

            //---- label3 ----
            label3.setText("Enter password:");
            panel1.add(label3, "cell 3 4");
            panel1.add(passwordField1, "cell 3 5");

            //---- enterAppBtn ----
            enterAppBtn.setText("Enter");
            panel1.add(enterAppBtn, "cell 3 7");
        }
        contentPane.add(panel1, "card1");
        pack();
        setLocationRelativeTo(getOwner());

        //======== dialog1 ========
        {
            dialog1.setTitle("Employee");
            dialog1.setEnabled(false);
            Container dialog1ContentPane = dialog1.getContentPane();
            dialog1ContentPane.setLayout(new MigLayout(
                    "hidemode 3",
                    // columns
                    "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]",
                    // rows
                    "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]"));

            //---- label11 ----
            label11.setText("Name:");
            dialog1ContentPane.add(label11, "cell 0 0");

            //---- label14 ----
            label14.setText("Password:");
            dialog1ContentPane.add(label14, "cell 3 0");
            dialog1ContentPane.add(textField2, "cell 0 1");
            dialog1ContentPane.add(passwordField2, "cell 3 1");

            //---- label12 ----
            label12.setText("CNP:");
            dialog1ContentPane.add(label12, "cell 0 2");

            //---- label15 ----
            label15.setText("Address:");
            dialog1ContentPane.add(label15, "cell 3 2");
            dialog1ContentPane.add(textField3, "cell 0 3");
            dialog1ContentPane.add(textField6, "cell 3 3");

            //---- label13 ----
            label13.setText("Email:");
            dialog1ContentPane.add(label13, "cell 0 4");

            //---- label16 ----
            label16.setText("Admin rights:");
            dialog1ContentPane.add(label16, "cell 3 4");
            dialog1ContentPane.add(textField4, "cell 0 5");
            dialog1ContentPane.add(textField7, "cell 3 5");

            //---- processEmployeeBtn ----
            processEmployeeBtn.setText("Process");
            dialog1ContentPane.add(processEmployeeBtn, "cell 5 5");
            dialog1.pack();
            dialog1.setLocationRelativeTo(dialog1.getOwner());
        }

        //======== dialog2 ========
        {
            dialog2.setTitle("Client");
            dialog2.setLayout(new MigLayout(
                    "hidemode 3",
                    // columns
                    "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]",
                    // rows
                    "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]"));

            //---- label17 ----
            label17.setText("Name:");
            dialog2.add(label17, "cell 0 0");

            //---- label20 ----
            label20.setText("Address");
            dialog2.add(label20, "cell 3 0");
            dialog2.add(textField8, "cell 0 1");
            dialog2.add(textField11, "cell 3 1");

            //---- label18 ----
            label18.setText("CNP:");
            dialog2.add(label18, "cell 0 2");

            //---- label19 ----
            label19.setText("Email:");
            dialog2.add(label19, "cell 3 2");
            dialog2.add(textField9, "cell 0 3");
            dialog2.add(textField10, "cell 3 3");

            //---- processClientBtn ----
            processClientBtn.setText("Process");
            dialog2.add(processClientBtn, "cell 7 5");
            dialog2.pack();
            dialog2.setLocationRelativeTo(dialog2.getOwner());
        }

        //======== dialog3 ========
        {
            dialog3.setTitle("Account");
            Container dialog3ContentPane = dialog3.getContentPane();
            dialog3ContentPane.setLayout(new MigLayout(
                    "hidemode 3",
                    // columns
                    "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]",
                    // rows
                    "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]"));

            //---- label21 ----
            label21.setText("AccNo:");
            dialog3ContentPane.add(label21, "cell 0 0 2 1");
            dialog3ContentPane.add(textField12, "cell 0 1 2 1");

            //---- label22 ----
            label22.setText("Money:");
            dialog3ContentPane.add(label22, "cell 0 2");
            dialog3ContentPane.add(textField13, "cell 0 3 2 1");

            //---- label23 ----
            label23.setText("Type:");
            dialog3ContentPane.add(label23, "cell 0 4");
            dialog3ContentPane.add(textField14, "cell 0 5 2 1");

            label27.setText("Personal Code");
            dialog3ContentPane.add(label27, "cell 6 0");
            dialog3ContentPane.add(textField12, "cell 0 1 2 1");
            dialog3ContentPane.add(textField5, "cell 6 1");

            //---- processAccountBtn ----
            processAccountBtn.setText("Process");
            dialog3ContentPane.add(processAccountBtn, "cell 6 5");
            dialog3.pack();
            dialog3.setLocationRelativeTo(dialog3.getOwner());
        }

        //======== dialog4 ========
        {
            dialog4.setTitle("Transfer");
            Container dialog4ContentPane = dialog4.getContentPane();
            dialog4ContentPane.setLayout(new MigLayout(
                    "hidemode 3",
                    // columns
                    "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]" +
                            "[fill]",
                    // rows
                    "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]" +
                            "[]"));

            //---- label24 ----
            label24.setText("Account number (from)");
            dialog4ContentPane.add(label24, "cell 0 0");

            //---- label31 ----
            label31.setText("Client CNP");
            dialog4ContentPane.add(label31, "cell 5 0");
            dialog4ContentPane.add(textField15, "cell 0 1");
            dialog4ContentPane.add(textField21, "cell 5 1");

            //---- label25 ----
            label25.setText("Account number (to)");
            dialog4ContentPane.add(label25, "cell 0 3");
            dialog4ContentPane.add(textField16, "cell 0 4");

            //---- label26 ----
            label26.setText("Amount");
            dialog4ContentPane.add(label26, "cell 0 6");
            dialog4ContentPane.add(textField17, "cell 0 7");

            //---- transferAccConfirmBtn ----
            transferAccConfirmBtn.setText("Transfer");
            dialog4ContentPane.add(transferAccConfirmBtn, "cell 6 7");

            //======== scrollPane3 ========
            {
                scrollPane3.setViewportView(textArea3);
            }
            dialog4ContentPane.add(scrollPane3, "cell 6 8");
            dialog4.pack();
            dialog4.setLocationRelativeTo(dialog4.getOwner());

            //======== dialog5 ========
            {
                Container dialog5ContentPane = dialog5.getContentPane();
                dialog5ContentPane.setLayout(new MigLayout(
                        "hidemode 3",
                        // columns
                        "[fill]" +
                                "[fill]" +
                                "[fill]" +
                                "[fill]" +
                                "[fill]" +
                                "[fill]" +
                                "[fill]" +
                                "[fill]",
                        // rows
                        "[]" +
                                "[]" +
                                "[]" +
                                "[]" +
                                "[]" +
                                "[]"));

                //---- label28 ----
                label28.setText("Employee CNP");
                dialog5ContentPane.add(label28, "cell 0 0");

                //---- label29 ----
                label29.setText("From");
                dialog5ContentPane.add(label29, "cell 3 0");

                //---- label30 ----
                label30.setText("To");
                dialog5ContentPane.add(label30, "cell 5 0");
                dialog5ContentPane.add(textField18, "cell 0 1,dock center");
                dialog5ContentPane.add(textField19, "cell 3 1,dock center");
                dialog5ContentPane.add(textField20, "cell 5 1 3 1,dock center");

                //---- button19 ----
                button19.setText("Generate");
                dialog5ContentPane.add(button19, "cell 3 5");
                dialog5.pack();
                dialog5.setLocationRelativeTo(dialog5.getOwner());
            }

            //======== dialog6 ========
            {
                dialog6.setTitle("Bill");
                Container dialog6ContentPane = dialog6.getContentPane();
                dialog6ContentPane.setLayout(new MigLayout(
                        "hidemode 3",
                        // columns
                        "[fill]" +
                                "[fill]" +
                                "[fill]" +
                                "[fill]" +
                                "[fill]" +
                                "[fill]" +
                                "[fill]",
                        // rows
                        "[]" +
                                "[]" +
                                "[]" +
                                "[]" +
                                "[]" +
                                "[]"));

                //---- label32 ----
                label32.setText("Client CNP");
                dialog6ContentPane.add(label32, "cell 0 0");
                dialog6ContentPane.add(textField22, "cell 0 1");

                //---- label33 ----
                label33.setText("Bill ID");
                dialog6ContentPane.add(label33, "cell 0 2");
                dialog6ContentPane.add(textField23, "cell 0 3");

                //---- label34 ----
                label34.setText("Account ID");
                dialog6ContentPane.add(label34, "cell 0 4");
                dialog6ContentPane.add(textField24, "cell 0 5");

                //---- button20 ----
                button20.setText("Pay bill");
                dialog6ContentPane.add(button20, "cell 6 5");
                dialog6.pack();
                dialog6.setLocationRelativeTo(dialog6.getOwner());
            }

            panel1.setVisible(true);
            panel2.setVisible(false);
            panel3.setVisible(false);
        }
    }

    private CardLayout windowLayout;
    private JPanel panel3;
    private JLabel label8;
    private JLabel label10;
    private JLabel label9;
    private JButton createEmployeeBtn;
    private JScrollPane scrollPane2;
    private JTextArea textArea2;
    private JButton updateEmployeeBtn;
    private JButton viewEmployeeBtn;
    private JButton deleteEmployeeBtn;
    private JButton generateReportBtn;
    private JPanel panel2;
    private JLabel label4;
    private JLabel label5;
    private JLabel label6;
    private JLabel label7;
    private JButton addClientBtn;
    private JButton addAccountBtn;
    private JButton updateClientBtn;
    private JButton updateAccountBtn;
    private JScrollPane scrollPane1;
    private JTextArea textArea1;
    private JButton viewClientBtn;
    private JButton viewAccountBtn;
    private JButton deleteAccountBtn;
    private JButton transferAccountBtn;
    private JPanel panel1;
    private JLabel label1;
    private JLabel label2;
    private JTextField textField1;
    private JLabel label3;
    private JPasswordField passwordField1;
    private JButton enterAppBtn;
    private JDialog dialog1;
    private JLabel label11;
    private JLabel label14;
    private JTextField textField2;
    private JPasswordField passwordField2;
    private JLabel label12;
    private JLabel label15;
    private JTextField textField3;
    private JTextField textField6;
    private JLabel label13;
    private JLabel label16;
    private JTextField textField4;
    private JTextField textField7;
    private JButton processEmployeeBtn;
    private Dialog dialog2;
    private JLabel label17;
    private JLabel label20;
    private JTextField textField8;
    private JTextField textField11;
    private JLabel label18;
    private JLabel label19;
    private JTextField textField9;
    private JTextField textField10;
    private JButton processClientBtn;
    private JDialog dialog3;
    private JLabel label21;
    private JTextField textField12;
    private JLabel label22;
    private JTextField textField13;
    private JLabel label23;
    private JTextField textField14;
    private JButton processAccountBtn;
    private JDialog dialog4;
    private JLabel label24;
    private JTextField textField15;
    private JLabel label25;
    private JTextField textField16;
    private JLabel label26;
    private JTextField textField17;
    private JButton transferAccConfirmBtn;
    private JScrollPane scrollPane3;
    private JTextArea textArea3;
    private JTextField textField5;
    private JLabel label27;
    private JDialog dialog5;
    private JLabel label28;
    private JLabel label29;
    private JLabel label30;
    private JTextField textField18;
    private JTextField textField19;
    private JTextField textField20;
    private JButton button19;
    private JLabel label31;
    private JTextField textField21;
    private JDialog dialog6;
    private JLabel label32;
    private JTextField textField22;
    private JLabel label33;
    private JTextField textField23;
    private JButton button20;
    private JButton button21;
    private JLabel label34;
    private JTextField textField24;

    public CardLayout getWindowLayout() {
        return windowLayout;
    }

    public void setWindowLayout(CardLayout windowLayout) {
        this.windowLayout = windowLayout;
    }

    public JPanel getPanel3() {
        return panel3;
    }

    public void setPanel3(JPanel panel3) {
        this.panel3 = panel3;
    }

    public JPanel getPanel2() {
        return panel2;
    }

    public void setPanel2(JPanel panel2) {
        this.panel2 = panel2;
    }

    public JPanel getPanel1() {
        return panel1;
    }

    public void setPanel1(JPanel panel1) {
        this.panel1 = panel1;
    }

    public JTextArea getTextArea2() {
        return textArea2;
    }

    public void setTextArea2(JTextArea textArea2) {
        this.textArea2 = textArea2;
    }

    public JTextArea getTextArea1() {
        return textArea1;
    }

    public void setTextArea1(JTextArea textArea1) {
        this.textArea1 = textArea1;
    }

    public JTextField getTextField1() {
        return textField1;
    }

    public void setTextField1(JTextField textField1) {
        this.textField1 = textField1;
    }

    public JPasswordField getPasswordField1() {
        return passwordField1;
    }

    public void setPasswordField1(JPasswordField passwordField1) {
        this.passwordField1 = passwordField1;
    }

    public JDialog getDialog1() {
        return dialog1;
    }

    public void setDialog1(JDialog dialog1) {
        this.dialog1 = dialog1;
    }

    public JTextField getTextField2() {
        return textField2;
    }

    public void setTextField2(JTextField textField2) {
        this.textField2 = textField2;
    }

    public JPasswordField getPasswordField2() {
        return passwordField2;
    }

    public void setPasswordField2(JPasswordField passwordField2) {
        this.passwordField2 = passwordField2;
    }

    public JTextField getTextField3() {
        return textField3;
    }

    public void setTextField3(JTextField textField3) {
        this.textField3 = textField3;
    }

    public JTextField getTextField6() {
        return textField6;
    }

    public void setTextField6(JTextField textField6) {
        this.textField6 = textField6;
    }

    public JTextField getTextField4() {
        return textField4;
    }

    public void setTextField4(JTextField textField4) {
        this.textField4 = textField4;
    }

    public JTextField getTextField7() {
        return textField7;
    }

    public void setTextField7(JTextField textField7) {
        this.textField7 = textField7;
    }

    public Dialog getDialog2() {
        return dialog2;
    }

    public void setDialog2(Dialog dialog2) {
        this.dialog2 = dialog2;
    }

    public JTextField getTextField8() {
        return textField8;
    }

    public void setTextField8(JTextField textField8) {
        this.textField8 = textField8;
    }

    public JTextField getTextField11() {
        return textField11;
    }

    public void setTextField11(JTextField textField11) {
        this.textField11 = textField11;
    }

    public JTextField getTextField9() {
        return textField9;
    }

    public void setTextField9(JTextField textField9) {
        this.textField9 = textField9;
    }

    public JTextField getTextField10() {
        return textField10;
    }

    public void setTextField10(JTextField textField10) {
        this.textField10 = textField10;
    }

    public JDialog getDialog3() {
        return dialog3;
    }

    public void setDialog3(JDialog dialog3) {
        this.dialog3 = dialog3;
    }

    public JTextField getTextField12() {
        return textField12;
    }

    public void setTextField12(JTextField textField12) {
        this.textField12 = textField12;
    }

    public JTextField getTextField13() {
        return textField13;
    }

    public void setTextField13(JTextField textField13) {
        this.textField13 = textField13;
    }

    public JTextField getTextField14() {
        return textField14;
    }

    public void setTextField14(JTextField textField14) {
        this.textField14 = textField14;
    }

    public JTextField getTextField15() {
        return textField15;
    }

    public void setTextField15(JTextField textField15) {
        this.textField15 = textField15;
    }

    public JTextField getTextField16() {
        return textField16;
    }

    public void setTextField16(JTextField textField16) {
        this.textField16 = textField16;
    }

    public JTextField getTextField17() {
        return textField17;
    }

    public void setTextField17(JTextField textField17) {
        this.textField17 = textField17;
    }

    public JTextArea getTextArea3() {
        return textArea3;
    }

    public JTextField getTextField5() {
        return textField5;
    }

    public void setTextField5(JTextField textField5) {
        this.textField5 = textField5;
    }

    public JDialog getDialog4() {
        return dialog4;
    }

    public void setDialog4(JDialog dialog4) {
        this.dialog4 = dialog4;
    }

    public JLabel getLabel8() {
        return label8;
    }

    public void setLabel8(JLabel label8) {
        this.label8 = label8;
    }

    public JTextField getTextField18() {
        return textField18;
    }

    public void setTextField18(JTextField textField18) {
        this.textField18 = textField18;
    }

    public JTextField getTextField19() {
        return textField19;
    }

    public void setTextField19(JTextField textField19) {
        this.textField19 = textField19;
    }

    public JTextField getTextField20() {
        return textField20;
    }

    public void setTextField20(JTextField textField20) {
        this.textField20 = textField20;
    }

    public void setTextArea3(JTextArea textArea3) {
        this.textArea3 = textArea3;
    }

    public JDialog getDialog5() {
        return dialog5;
    }

    public void setDialog5(JDialog dialog5) {
        this.dialog5 = dialog5;
    }

    public JTextField getTextField21() {
        return textField21;
    }

    public void setTextField21(JTextField textField21) {
        this.textField21 = textField21;
    }

    public JLabel getLabel10() {
        return label10;
    }

    public void setLabel10(JLabel label10) {
        this.label10 = label10;
    }

    public JDialog getDialog6() {
        return dialog6;
    }

    public void setDialog6(JDialog dialog6) {
        this.dialog6 = dialog6;
    }

    public JTextField getTextField22() {
        return textField22;
    }

    public void setTextField22(JTextField textField22) {
        this.textField22 = textField22;
    }

    public JTextField getTextField23() {
        return textField23;
    }

    public void setTextField23(JTextField textField23) {
        this.textField23 = textField23;
    }

    public JTextField getTextField24() {
        return textField24;
    }

    public void setTextField24(JTextField textField24) {
        this.textField24 = textField24;
    }

    public void enterAppListener(ActionListener a) {
        enterAppBtn.addActionListener(a);
    }

    public void addAccountListener(ActionListener a) {
        addAccountBtn.addActionListener(a);
    }

    public void viewAccountListener(ActionListener a) {
        viewAccountBtn.addActionListener(a);
    }

    public void updateAccountListener(ActionListener a) {
        updateAccountBtn.addActionListener(a);
    }

    public void deleteAccountListener(ActionListener a) {
        deleteAccountBtn.addActionListener(a);
    }

    public void transferMoneyListener(ActionListener a) {
        transferAccountBtn.addActionListener(a);
    }

    public void addClientListener(ActionListener a) {
        addClientBtn.addActionListener(a);
    }

    public void updateClientListener(ActionListener a) {
        updateClientBtn.addActionListener(a);
    }

    public void viewClientListener(ActionListener a) {
        viewClientBtn.addActionListener(a);
    }

    public void addEmployeeListener(ActionListener a) {
        createEmployeeBtn.addActionListener(a);
    }

    public void viewEmployeeListener(ActionListener a) {
        viewEmployeeBtn.addActionListener(a);
    }

    public void updateEmployeelistener(ActionListener a) {
        updateEmployeeBtn.addActionListener(a);
    }

    public void deleteEmployeeListener(ActionListener a) {
        deleteEmployeeBtn.addActionListener(a);
    }

    public void generateReportListener(ActionListener a) {
        generateReportBtn.addActionListener(a);
    }

    public void generateConfirmListener(ActionListener a) { button19.addActionListener(a); }

    public void processClientListener(ActionListener a) {
        processClientBtn.addActionListener(a);
    }

    public void processAccountListener(ActionListener a) {
        processAccountBtn.addActionListener(a);
    }

    public void processEmployeeListener(ActionListener a) {
        processEmployeeBtn.addActionListener(a);
    }

    public void transferAccConfirmListener(ActionListener a) {
        transferAccConfirmBtn.addActionListener(a);
    }

    public void processBillListener(ActionListener a) {
        button21.addActionListener(a);
    }

    public void payBillListener(ActionListener a) {
        button20.addActionListener(a);
    }
}

