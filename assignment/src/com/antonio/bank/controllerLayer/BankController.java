package com.antonio.bank.controllerLayer;

import com.antonio.bank.businessLogicLayer.BankRegistry;
import com.antonio.bank.dataSourceLayer.*;
import com.antonio.bank.presentationLayer.BankGUI;

import java.awt.*;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Antonio on 3/25/2017.
 */
public class BankController {

    BankGUI bankGUI;
    private boolean viewPressed = false;
    private boolean addPressed = false;
    private boolean updatePressed = false;
    private boolean deletePressed = false;
    private boolean generatePressed = true;
    private Employee currentEmployee;

    public BankController(BankGUI bankGUI) {
        this.bankGUI = bankGUI;

        bankGUI.enterAppListener(new enterAppListener());
        bankGUI.addClientListener(new addCListener());
        bankGUI.updateClientListener(new updateCListener());
        bankGUI.viewClientListener(new viewCListener());
        bankGUI.addAccountListener(new addAListener());
        bankGUI.viewAccountListener(new viewAListener());
        bankGUI.updateAccountListener(new updateAListener());
        bankGUI.deleteAccountListener(new deleteAListener());
        bankGUI.transferMoneyListener(new transferAListener());
        bankGUI.addEmployeeListener(new addEListener());
        bankGUI.updateEmployeelistener(new updateEListener());
        bankGUI.viewEmployeeListener(new viewEListener());
        bankGUI.deleteEmployeeListener(new deleteEListener());
        bankGUI.generateReportListener(new generateREListener());
        bankGUI.processAccountListener(new processAListener());
        bankGUI.processClientListener(new processCListener());
        bankGUI.processEmployeeListener(new processEListener());
        bankGUI.transferAccConfirmListener(new transferAConfirmListener());
        bankGUI.generateConfirmListener(new generateCListener());
        bankGUI.processBillListener(new processBListener());
        bankGUI.payBillListener(new payBListener());
    }

    class enterAppListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            currentEmployee = BankRegistry.getEmployeeByUser(bankGUI.getTextField1().getText());
            String password = String.valueOf(bankGUI.getPasswordField1().getPassword());
            System.out.println(password);
            System.out.println(currentEmployee.getPassword());
            if (currentEmployee.getPassword().equals(password)) {
                if (currentEmployee.getAdminRights().equals(true)) {
                    bankGUI.getPanel1().setVisible(false);
                    bankGUI.getPanel3().setVisible(true);
                    bankGUI.getPanel2().setVisible(false);
                } else if (currentEmployee.getAdminRights().equals(false)) {
                    bankGUI.getPanel1().setVisible(false);
                    bankGUI.getPanel3().setVisible(false);
                    bankGUI.getPanel2().setVisible(true);
                }
            } else {
                System.out.println("Wrong password");
            }
        }
    }

    class addCListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            addPressed = true;
            bankGUI.getDialog2().setVisible(true);
            bankGUI.getDialog2().setEnabled(true);
            bankGUI.getDialog2().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);

        }
    }

    class viewCListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            viewPressed = true;
            bankGUI.getDialog2().setVisible(true);
            bankGUI.getDialog2().setEnabled(true);
            bankGUI.getDialog2().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        }
    }

    class updateCListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            updatePressed = true;
            bankGUI.getDialog2().setVisible(true);
            bankGUI.getDialog2().setEnabled(true);
            bankGUI.getDialog2().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        }
    }

    class processBListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            bankGUI.getDialog6().setVisible(true);
            bankGUI.getDialog6().setEnabled(true);
            bankGUI.getDialog6().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        }
    }

    class payBListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Bill processedBill = null;
            Account payingAccount = null;
            try {
                processedBill = Bill.find(bankGUI.getTextField23().getText());
                System.out.println(processedBill.getAmount());
                payingAccount = Account.find(bankGUI.getTextField24().getText(), "jdbc:sqlite:test.db");
                System.out.println(payingAccount.getMoneyAmount());
                if(payingAccount != null && processedBill != null) {
                    if(payingAccount.getMoneyAmount() > processedBill.getAmount()) {
                        payingAccount.subtract(processedBill.getAmount());
                        System.out.println(payingAccount.getMoneyAmount());
                        payingAccount.update("jdbc:sqlite:test.db");

                        LocalDate localDate = LocalDate.now();
                        try {
                            Report newReport = new Report("Paid bill " + processedBill.getBillID() + " using account " +
                                    payingAccount.getAccNo(), localDate);
                            newReport.insert(currentEmployee.getPersonalCode());
                        } catch (SQLException ex) {
                        }

                        processedBill.processBill(bankGUI.getTextField22().getText());
                        bankGUI.getTextArea1().setText("Bill paid!!!");
                    } else
                        bankGUI.getTextArea1().setText("Not enough money to pay bill !!!");
                } else {
                    if (payingAccount == null)
                        bankGUI.getTextArea1().setText("Account does not exist !!!");
                    if (processedBill == null)
                        bankGUI.getTextArea1().setText("Bill does not exist !!!");
                }
            } catch (SQLException ex) {

            }
            bankGUI.getDialog6().setVisible(false);
        }
    }

    class addAListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            addPressed = true;
            bankGUI.getDialog3().setVisible(true);
            bankGUI.getDialog3().setEnabled(true);
            bankGUI.getDialog3().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        }
    }

    class viewAListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            viewPressed = true;
            bankGUI.getDialog3().setVisible(true);
            bankGUI.getDialog3().setEnabled(true);
            bankGUI.getDialog3().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        }
    }

    class updateAListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            updatePressed = true;
            bankGUI.getDialog3().setVisible(true);
            bankGUI.getDialog3().setEnabled(true);
            bankGUI.getDialog3().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        }
    }

    class deleteAListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            deletePressed = true;
            bankGUI.getDialog3().setVisible(true);
            bankGUI.getDialog3().setEnabled(true);
            bankGUI.getDialog3().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        }
    }

    class transferAListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            bankGUI.getDialog4().setVisible(true);
            bankGUI.getDialog4().setEnabled(true);
            bankGUI.getDialog4().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        }
    }

    class addEListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            addPressed = true;
            bankGUI.getDialog1().setVisible(true);
            bankGUI.getDialog1().setEnabled(true);
            bankGUI.getDialog1().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        }
    }

    class viewEListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            viewPressed = true;
            bankGUI.getDialog1().setVisible(true);
            bankGUI.getDialog1().setEnabled(true);
            bankGUI.getDialog1().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        }
    }

    class updateEListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            updatePressed = true;
            bankGUI.getDialog1().setVisible(true);
            bankGUI.getDialog1().setEnabled(true);
            bankGUI.getDialog1().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        }
    }

    class deleteEListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            deletePressed = true;
            bankGUI.getDialog1().setVisible(true);
            bankGUI.getDialog1().setEnabled(true);
            bankGUI.getDialog1().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        }
    }

    class generateREListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            generatePressed = true;
            bankGUI.getDialog5().setVisible(true);
            bankGUI.getDialog5().setEnabled(true);
            bankGUI.getDialog5().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        }
    }

    class transferAConfirmListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                boolean state = false;
                if (Account.find(bankGUI.getTextField15().getText(), "jdbc:sqlite:test.db").getMoneyAmount() >
                        Double.parseDouble(bankGUI.getTextField17().getText())
                        && checkMoneyAmount( Double.parseDouble(bankGUI.getTextField17().getText()))) {
                    state = Account.transferMoney(bankGUI.getTextField21().getText(), bankGUI.getTextField15().getText(),
                            bankGUI.getTextField16().getText(), Double.parseDouble(bankGUI.getTextField17().getText()));
                    bankGUI.getTextArea1().setText("Money transfered !!!");
                    LocalDate localDate = LocalDate.now();
                    Report newReport = new Report("Transfered money ", localDate);
                    newReport.insert(currentEmployee.getPersonalCode());
                } else if(!state)
                    bankGUI.getTextArea1().setText("Input incorrect !!!");
                } catch(SQLException ex) {}

            bankGUI.getDialog4().setVisible(false);
            bankGUI.getTextField21().setText("");
            bankGUI.getTextField15().setText("");
            bankGUI.getTextField16().setText("");
            bankGUI.getTextField17().setText("");
        }
    }

    class processCListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (addPressed) {
                Client newClient = null;
                if(checkInputName(bankGUI.getTextField8().getText()) &&
                        checkInputPersonalCode(bankGUI.getTextField9().getText()) &&
                        checkMail(bankGUI.getTextField10().getText())) {
                    newClient = new Client(bankGUI.getTextField8().getText(), bankGUI.getTextField9().getText(),
                            bankGUI.getTextField10().getText(), bankGUI.getTextField11().getText());
                    try {
                        if (newClient != null)
                            newClient.insert("jdbc:sqlite:test.db");
                    } catch (SQLException ex) {

                    }
                    bankGUI.getTextArea1().setText("Client added !!!");
                } else
                    bankGUI.getTextArea1().setText("Input is incorrect !");
                addPressed = false;
                bankGUI.getDialog2().setVisible(false);
                bankGUI.getTextField8().setText("");
                bankGUI.getTextField9().setText("");
                bankGUI.getTextField10().setText("");
                bankGUI.getTextField11().setText("");

                LocalDate localDate = LocalDate.now();
                try {
                    if(newClient != null) {
                        Report newReport = new Report("Added client: " + newClient.toString(), localDate);
                        newReport.insert(currentEmployee.getPersonalCode());
                    }
                } catch(SQLException ex) {}

            }
            if (viewPressed) {
                Client viewedClient = null;
                try {
                    viewedClient = Client.find(bankGUI.getTextField9().getText(), "jdbc:sqlite:test.db");
                    if (viewedClient != null) {
                        bankGUI.getTextArea1().setText(viewedClient.toString());
                    } else {
                        bankGUI.getTextArea1().setText("Client doesn't exist!");

                    }
                    bankGUI.getTextField9().setText("");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                viewPressed = false;
                bankGUI.getDialog2().setVisible(false);

                LocalDate localDate = LocalDate.now();

                if (viewedClient != null) {
                    try {
                        Report newReport = new Report("Viewed client: " + viewedClient.toString(), localDate);
                        newReport.insert(currentEmployee.getPersonalCode());
                    } catch(SQLException ex) {}
                }

            }
            if (updatePressed) {
                Client newClient = null;

                try {
                    newClient = Client.find(bankGUI.getTextField9().getText(), "jdbc:sqlite:test.db");

                    if (!bankGUI.getTextField8().getText().isEmpty())
                        if(checkInputName(bankGUI.getTextField8().getText()))
                        newClient.setName(bankGUI.getTextField8().getText());
                        else
                            bankGUI.getTextArea1().setText("Name is incorrect!");
                    if (!bankGUI.getTextField10().getText().isEmpty())
                        if(checkMail(bankGUI.getTextField10().getText()))
                        newClient.setEmail(bankGUI.getTextField10().getText());
                        else
                            bankGUI.getTextArea1().setText("Mail is incorrect!");
                    if (!bankGUI.getTextField11().getText().isEmpty())
                        newClient.setAddress(bankGUI.getTextField11().getText());
                    newClient.update();
                    bankGUI.getTextArea1().setText(newClient.toString());
                } catch (SQLException ex) {

                }
                bankGUI.getTextField8().setText("");
                bankGUI.getTextField9().setText("");
                bankGUI.getTextField10().setText("");
                bankGUI.getTextField11().setText("");
                updatePressed = false;
                bankGUI.getDialog2().setVisible(false);

                LocalDate localDate = LocalDate.now();

                if (newClient != null) {
                    try {
                        Report newReport = new Report("Updated client: " + newClient.toString(), localDate);
                        newReport.insert(currentEmployee.getPersonalCode());
                    } catch(SQLException ex) {}
                }

            }
        }
    }

    class processAListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (addPressed) {
                LocalDate localDate = LocalDate.now();

                Account newAccount = null;
                if(checkAccType(bankGUI.getTextField14().getText()) &&
                        checkMoneyAmount(Double.parseDouble(bankGUI.getTextField13().getText()))) {
                    newAccount = new Account(bankGUI.getTextField12().getText(), bankGUI.getTextField14().getText(),
                            Double.parseDouble(bankGUI.getTextField13().getText()), localDate.toString());

                    try {
                        if (newAccount != null)
                            newAccount.insert(bankGUI.getTextField5().getText(), "jdbc:sqlite:test.db");
                    } catch (SQLException ex) {

                    }
                    bankGUI.getTextArea1().setText("Account added !");
                } else
                    bankGUI.getTextArea1().setText("Input is incorrect !");
                addPressed = false;
                bankGUI.getDialog3().setVisible(false);

                LocalDate localDateTime = LocalDate.now();


                try {
                    if(newAccount != null) {
                        Report newReport = new Report("Added account: " + newAccount.toString(), localDateTime);
                        newReport.insert(currentEmployee.getPersonalCode());
                    }
                } catch(SQLException ex) {}

            }
            if (viewPressed) {
                Account viewedAccount = null;
                try {
                    viewedAccount = Account.find(bankGUI.getTextField12().getText(), "jdbc:sqlite:test.db");
                    if(viewedAccount != null)
                    bankGUI.getTextArea1().setText(viewedAccount.toString());
                    else
                        bankGUI.getTextArea1().setText("Account does not exist !!!");
                } catch (SQLException ex) {

                }
                viewPressed = false;
                bankGUI.getDialog3().setVisible(false);
                bankGUI.getTextField12().setText("");
                bankGUI.getTextField13().setText("");
                bankGUI.getTextField14().setText("");
                LocalDate localDateTime = LocalDate.now();

                if (viewedAccount != null)
                    try {
                        Report newReport = new Report("Viewed account: " + viewedAccount.toString(), localDateTime);
                        newReport.insert(currentEmployee.getPersonalCode());
                    } catch(SQLException ex) {}
            }
            if (updatePressed) {
                Account updatedAccount = null;
                try {
                    updatedAccount = Account.find(bankGUI.getTextField12().getText(), "jdbc:sqlite:test.db");
                    if(!bankGUI.getTextField13().getText().isEmpty()) {
                        if (checkMoneyAmount(Double.parseDouble(bankGUI.getTextField13().getText())))
                            updatedAccount.setMoneyAmount(Double.parseDouble(bankGUI.getTextField13().getText()));
                        else
                            bankGUI.getTextArea1().setText("Money amount is incorrect!!!");
                    }
                    updatedAccount.update("jdbc:sqlite:test.db");
                    bankGUI.getTextArea1().setText("Money amount updated !!!");
                } catch (SQLException ex) {

                }
                updatePressed = false;
                bankGUI.getDialog3().setVisible(false);
                bankGUI.getTextField5().setText("");
                bankGUI.getTextField14().setText("");

                LocalDate localDateTime = LocalDate.now();

                try {
                    Report newReport = new Report("Updated account: " + updatedAccount.toString(), localDateTime);
                    newReport.insert(currentEmployee.getPersonalCode());
                } catch(SQLException ex) {}
            }
            if (deletePressed) {
                try {
                    Account deletedAccount = Account.find(bankGUI.getTextField12().getText(), "jdbc:sqlite:test.db");

                    LocalDate localDateTime = LocalDate.now();

                    try {
                        if(deletedAccount != null) {
                            Report newReport = new Report("Deleted account: " + deletedAccount.toString(), localDateTime);
                            newReport.insert(currentEmployee.getPersonalCode());
                        }
                    } catch(SQLException ex) {}
                    if(deletedAccount != null) {
                        deletedAccount.delete(bankGUI.getTextField5().getText(), "jdbc:sqlite:test.db");
                        bankGUI.getTextArea1().setText("Account deleted !!!");
                    } else
                        bankGUI.getTextArea1().setText("Account does not exist !!!");
                } catch (SQLException ex) {

                }
                deletePressed = false;
                bankGUI.getDialog3().setVisible(false);
                bankGUI.getTextField5().setText("");
                bankGUI.getTextField12().setText("");


            }
        }
    }

    class processEListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (addPressed) {
                Employee newEmployee = null;

                if(!bankGUI.getTextField2().getText().isEmpty() && !bankGUI.getTextField3().getText().isEmpty() &&
                        !bankGUI.getTextField4().getText().isEmpty() && !bankGUI.getTextField6().getText().isEmpty() &&
                        !String.valueOf(bankGUI.getPasswordField2().getPassword()).isEmpty() &&
                        !bankGUI.getTextField7().getText().isEmpty()) {
                    if (checkInputName(bankGUI.getTextField2().getText()) &&
                            checkInputPersonalCode(bankGUI.getTextField3().getText()) &&
                            checkMail(bankGUI.getTextField4().getText()))
                        newEmployee = new Employee(bankGUI.getTextField2().getText(), bankGUI.getTextField3().getText(),
                                bankGUI.getTextField4().getText(), bankGUI.getTextField6().getText(),
                                String.valueOf(bankGUI.getPasswordField2().getPassword()),
                                Boolean.parseBoolean(bankGUI.getTextField7().getText()));
                } else
                    bankGUI.getTextArea2().setText("Input is incorrect !!!");
                try {
                    if(newEmployee != null)
                    newEmployee.insert();
                } catch (SQLException ex) {

                }
                if(newEmployee != null)
                bankGUI.getTextArea2().setText("Employee added !!!");

                addPressed = false;
                bankGUI.getDialog1().setVisible(false);
                bankGUI.getTextField2().setText("");
                bankGUI.getTextField3().setText("");
                bankGUI.getTextField4().setText("");
                bankGUI.getTextField6().setText("");
                bankGUI.getPasswordField2().setText("");
                bankGUI.getTextField7().setText("");
            } else if (viewPressed) {
                try {
                    Employee viewedEmployee = Employee.find(bankGUI.getTextField3().getText());
                    if(viewedEmployee != null)
                    bankGUI.getTextArea2().setText(viewedEmployee.toString());
                    else
                        bankGUI.getTextArea2().setText("Employee does not exist !!!");
                    bankGUI.getTextField9().setText("");
                } catch (SQLException ex) {

                }
                viewPressed = false;
                bankGUI.getDialog1().setVisible(false);
            } else if (updatePressed) {


                try {
                    Employee newEmployee = Employee.find(bankGUI.getTextField3().getText());
                    if(!bankGUI.getTextField2().getText().isEmpty()) {
                        if (checkInputName(bankGUI.getTextField2().getText()))
                            newEmployee.setName(bankGUI.getTextField2().getText());
                        else
                            bankGUI.getTextArea2().setText("Name is incorrect !!!");
                    }
                    if(!bankGUI.getTextField3().getText().isEmpty())
                        if(checkInputPersonalCode(bankGUI.getTextField3().getText()))
                    newEmployee.setPersonalCode(bankGUI.getTextField3().getText());
                        else
                            bankGUI.getTextArea2().setText("Personal code is incorrect !!!");
                    if(!bankGUI.getTextField4().getText().isEmpty()) {
                        if(checkMail(bankGUI.getTextField4().getText()))
                        newEmployee.setEmail(bankGUI.getTextField4().getText());
                        else
                        bankGUI.getTextArea2().setText("Email is incorrect !!!");
                    }
                    if(!bankGUI.getTextField6().getText().isEmpty())
                        newEmployee.setAddress(bankGUI.getTextField6().getText());
                    if(!String.valueOf(bankGUI.getPasswordField2().getPassword()).isEmpty())
                        newEmployee.setPassword(String.valueOf(bankGUI.getPasswordField2().getPassword()));
                    else
                        bankGUI.getTextArea2().setText("Password is empty !!!");
                    if(!bankGUI.getTextField7().getText().isEmpty())
                        newEmployee.setAdminRights(Boolean.parseBoolean(bankGUI.getTextField7().getText()));
                    newEmployee.update();
                    bankGUI.getTextArea2().setText("Employee updated !!!");
                } catch (SQLException ex) {

                }
                bankGUI.getTextField2().setText("");
                bankGUI.getTextField3().setText("");
                bankGUI.getTextField4().setText("");
                bankGUI.getTextField6().setText("");
                bankGUI.getPasswordField2().setText("");
                bankGUI.getTextField7().setText("");
                updatePressed = false;
                bankGUI.getDialog1().setVisible(false);
            } else if (deletePressed) {
                try {
                    Employee deletedEmployee = Employee.find(bankGUI.getTextField3().getText());
                    if(deletedEmployee != null) {
                        deletedEmployee.delete();
                        bankGUI.getTextArea2().setText("Employee deleted !!!");
                    } else
                        bankGUI.getTextArea2().setText("Employee does not exist !!!");
                } catch (SQLException ex) {

                }
                deletePressed = false;
                bankGUI.getDialog1().setVisible(false);
                bankGUI.getTextField3().setText("");

            }
        }
    }

    class generateCListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            String activity = "";
            try {
                Employee employee = Employee.find(bankGUI.getTextField18().getText());

                LocalDate dateFrom = LocalDate.parse(bankGUI.getTextField19().getText());
                LocalDate dateTo = LocalDate.parse(bankGUI.getTextField20().getText());

                if(employee != null) {
                    List<Report> reports = Report.generate(dateFrom, dateTo, employee.getPersonalCode());

                    for (Report r : reports)
                        activity += "\n\n" + r.getActivity();

                    bankGUI.getTextArea2().setText(activity);
                } else
                    bankGUI.getTextArea2().setText("Employee does not exist !!!");
            } catch (SQLException ex) {

            }
            bankGUI.getDialog5().setVisible(false);
            bankGUI.getTextField18().setText("");
            bankGUI.getTextField19().setText("");
            bankGUI.getTextField20().setText("");
        }
    }

    public boolean checkInputName(String name) {
        if(name.matches(".*\\d+.*")) {
            System.out.println("Wrong name!!!");
            return false;
        }
        else return true;
    }

    public boolean checkInputPersonalCode(String personalCode) {
        Pattern p = Pattern.compile("[^A-Za-z0-9]*$");
        Matcher m = p.matcher(personalCode);
        if(personalCode.length() == 13 && m.find()) {
            System.out.println("Personal code is correct!!!");
            return true;
        }
        else return false;
    }

    public boolean checkMail(String email) {
        if(email.contains("@")) {
            System.out.println("Email is correct!!!");
            return true;
        }
        else return false;
    }

    public boolean checkMoneyAmount(Double moneyAmount) {
        if(moneyAmount <= 0) {
            System.out.println("Negative money amount!!!");
            return false;
        } else return true;
    }

    public boolean checkAccType(String accType) {
        if(accType.toLowerCase().equals("cont de economii") ||
           accType.toLowerCase().equals("depozit la termen")) {
            System.out.println("Wrong account type!!!");
            return true;
        }
        else return false;
    }


}
