package com.antonio.bank.dataSourceLayer;

import com.antonio.bank.businessLogicLayer.BankRegistry;

import java.sql.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Antonio on 3/18/2017.
 */

public class Account {

    private Long accountID;
    private String accNo;
    private String type;
    private Double moneyAmount;
    private String date;

    private final static AtomicLong count = new AtomicLong(0);

    private final static String findStatementString =
            "SELECT Account_ID, AccNo, Type, Money, Date, Personal_Code" +
                    " FROM Account" +
                    " WHERE AccNo = ?";

    private final static String findPersonalCodeString =
            "SELECT Personal_Code" +
                    "FROM Account" +
                    "WHERE AccNo = ?";

    private final static String populateDBString =
            "SELECT * FROM ACCOUNT";

    private final static String updateStatementString =
            "UPDATE Account" +
                    " set Money = ?" +
                    " where AccNo = ?";

    private final static String insertStatementString =
            "INSERT INTO Account VALUES (?, ?, ?, ?, ?, ?)";

    private final static String deleteStatementString =
            "DELETE FROM Account " +
                    " where AccNo = ?";


    public Account(Long accountID, String accNo, String type, Double moneyAmount, String date) {
        this.accountID = accountID;
        this.accNo = accNo;
        this.type = type;
        this.moneyAmount = moneyAmount;
        this.date = date;
    }

    public Account(String accNo, String type, Double moneyAmount, String date) {
        this.accNo = accNo;
        this.type = type;
        this.moneyAmount = moneyAmount;
        this.date = date;
    }

    public static void populateDb() {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement populateDBStmt = null;
        ResultSet rs = null;

        try {
            populateDBStmt = c.prepareStatement(populateDBString);
            rs = populateDBStmt.executeQuery();

            while(rs != null) {
                count.getAndAdd(1);
                rs.next();
                load(rs);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(populateDBStmt != null)
                    populateDBStmt.close();
                c.close();
            } catch(SQLException ex) {}
        }

    }

    public static Account find(String id, String dbConnection) throws SQLException {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dbConnection);
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        Account result = BankRegistry.getAccount(id);
        if (result != null)
            return result;
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try {
            findStatement = c.prepareStatement(findStatementString);
            findStatement.setString(1, id);
            rs = findStatement.executeQuery();
            rs.next();
            result = load(rs);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(rs != null)
                rs.close();
            if(findStatement != null)
            findStatement.close();
            c.close();
        }
        return null;
    }

    public static Account load(ResultSet rs) throws SQLException {

        String accNo = rs.getString(2);
        Account result = BankRegistry.getAccount(accNo);
        if (result != null) return result;
        Long accountID = new Long(rs.getLong(1));
        String type = rs.getString(3);
        Double moneyAmount = rs.getDouble(4);
        String date = rs.getString(5);
        String clientID = rs.getString( 6);

        result = new Account(accountID, accNo, type, moneyAmount, date);
        BankRegistry.registerAccount(clientID, result);
        return result;
    }

    public void update(String dbConnection) throws SQLException {

        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dbConnection);
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement updateStatement = null;

        try {
            updateStatement = c.prepareStatement(updateStatementString);
            updateStatement.setDouble(1, getMoneyAmount());
            updateStatement.setString(2, accNo);
            updateStatement.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(updateStatement != null)
            updateStatement.close();
            c.close();
        }
    }

    public void delete(String clientID, String dbConnection) throws SQLException {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dbConnection);
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = c.prepareStatement(deleteStatementString);
            deleteStatement.setString(1, accNo);
            deleteStatement.execute();
            BankRegistry.removeAccount(clientID, this);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(deleteStatement != null)
            deleteStatement.close();
            c.close();
        }
    }

    public Long insert(String personalCodeString, String dbConnection) throws SQLException {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dbConnection);
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement insertStatement = null;
        try {
            insertStatement = c.prepareStatement(insertStatementString);

            setAccountID(count.incrementAndGet());
            insertStatement.setInt(1, getAccountID().intValue());
            insertStatement.setString(2, accNo);
            insertStatement.setString(3, type);
            insertStatement.setDouble(4, moneyAmount);
            insertStatement.setString(5, date);
            insertStatement.setString(6, personalCodeString);
            insertStatement.execute();
            BankRegistry.registerAccount(personalCodeString, this);
            return getAccountID();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(insertStatement != null)
            insertStatement.close();
            c.close();
        }
        return null;
    }

    public Long getAccountID() {
        return accountID;
    }

    public void setAccountID(Long accountID) {
        this.accountID = accountID;
    }

    public String getAccNo() {
        return accNo;
    }

    public Double getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(Double moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public String getAccountType() {
        return type;
    }

    public static boolean transferMoney(String clientID, String accountNoFrom, String accountNoTo, Double moneyAmount) {
        Connection c = null;
        boolean state = false;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement findPersonalCodeStmt = null;
        ResultSet rs = null;

        try {
            c.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
                Account accFrom = find(accountNoFrom, "jdbc:sqlite:test.db");
                Account accTo = find(accountNoTo, "jdbc:sqlite:test.db");

                if(accFrom != null && accTo != null) {
                    Double currentMoney = accFrom.getMoneyAmount();
                    accFrom.setMoneyAmount(currentMoney - moneyAmount);


                    accFrom.update("jdbc:sqlite:test.db");

                    currentMoney = accTo.getMoneyAmount();
                    accTo.setMoneyAmount(currentMoney + moneyAmount);

                    System.out.println(accTo.getMoneyAmount());

                    accTo.update("jdbc:sqlite:test.db");
                    state = true;
                } else
                    state = false;

            } catch(SQLException ex) {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            ex.printStackTrace();
            } finally {
                try {
                    if(rs != null)
                    rs.close();
                    if(findPersonalCodeStmt != null)
                    findPersonalCodeStmt.close();
                    c.commit();
                    c.setAutoCommit(true);
                    c.close();
                    return state;
                } catch(SQLException ex) {
                    ex.printStackTrace();
                }
            }
            return state;
    }

    public void subtract(Double amount) {
        setMoneyAmount(moneyAmount - amount);
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountID=" + accountID +
                ", accNo='" + accNo + '\'' +
                ", type='" + type + '\'' +
                ", moneyAmount=" + moneyAmount +
                ", date='" + date + '\'' +
                '}';
    }
}