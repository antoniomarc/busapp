package com.antonio.bank.dataSourceLayer;

import com.antonio.bank.businessLogicLayer.BankRegistry;

import java.sql.*;
import java.util.concurrent.atomic.AtomicLong;

/**
     * Created by Antonio on 3/20/2017.
     */
    public class Bill {
        private Long billID;
        private Double amount;

        private static AtomicLong count = new AtomicLong(0);

        private final static String findStatementString =
                "SELECT Bill_ID, amount, Personal_Code" +
                        " FROM Bill" +
                        " WHERE Bill_ID = ?";

        private final static String populateDBString =
                "SELECT * FROM Bill";



        private final static String deleteStatementString =
                "DELETE FROM Bill " +
                        " where Bill_ID = ?";


        public Bill(Long billID, Double amount) {
            this.billID = billID;
            this.amount = amount;
        }

        public static void populateDb() {
            Connection c = null;
            try {
                Class.forName("org.sqlite.JDBC");
                c = DriverManager.getConnection("jdbc:sqlite:test.db");
            } catch ( Exception e ) {
                System.err.println( e.getClass().getName() + ": " + e.getMessage() );
                System.exit(0);
            }

            PreparedStatement populateDBStmt = null;
            ResultSet rs = null;

            try {
                populateDBStmt = c.prepareStatement(populateDBString);
                rs = populateDBStmt.executeQuery();

                while(rs != null) {
                    count.getAndAdd(1);
                    rs.next();
                    load(rs);
                }
            } catch(SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if(rs != null)
                        rs.close();
                    if(populateDBStmt != null)
                        populateDBStmt.close();
                    c.close();
                } catch(SQLException ex) {
                    ex.printStackTrace();
                }
            }

        }

        public static Bill find(String id) throws SQLException {
            Connection c = null;
            try {
                Class.forName("org.sqlite.JDBC");
                c = DriverManager.getConnection("jdbc:sqlite:test.db");
            } catch ( Exception e ) {
                System.err.println( e.getClass().getName() + ": " + e.getMessage() );
                System.exit(0);
            }

            Bill result = BankRegistry.getBill(id);
            if (result != null)
                return result;
            PreparedStatement findStatement = null;
            ResultSet rs = null;

            try {
                findStatement = c.prepareStatement(findStatementString);
                findStatement.setLong(1, Long.parseLong(id));
                rs = findStatement.executeQuery();
                rs.next();
                result = load(rs);
                return result;
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if(rs != null)
                rs.close();
                if(findStatement != null)
                findStatement.close();
                c.close();
            }
            return null;
        }

        public static Bill load(ResultSet rs) throws SQLException {

            Long billID = new Long(rs.getLong(1));
            Bill result = BankRegistry.getBill(billID.toString());
            if (result != null) return result;

            System.out.println(billID);
            Double amount = rs.getDouble(2);
            String clientID = rs.getString( 3);
            result = new Bill(billID, amount);
            BankRegistry.registerBill(clientID, result);
            return result;
        }

        public void processBill(String clientID) {
            Connection c = null;
            try {
                Class.forName("org.sqlite.JDBC");
                c = DriverManager.getConnection("jdbc:sqlite:test.db");
            } catch ( Exception e ) {
                System.err.println( e.getClass().getName() + ": " + e.getMessage() );
                System.exit(0);
            }

            PreparedStatement deleteStatement = null;
            try {
                deleteStatement = c.prepareStatement(deleteStatementString);
                deleteStatement.setLong(1, billID);
                deleteStatement.execute();
                BankRegistry.removeBill(clientID, this);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if(deleteStatement != null)
                    deleteStatement.close();
                    c.close();
                } catch(SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

        public Long getBillID() {
            return billID;
        }

        public Double getAmount() {
            return amount;
        }
    }
