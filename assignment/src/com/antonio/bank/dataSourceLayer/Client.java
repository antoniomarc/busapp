package com.antonio.bank.dataSourceLayer;

import com.antonio.bank.businessLogicLayer.BankRegistry;

import java.sql.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Antonio on 3/18/2017.
 */
public class Client {

   private Long userID;
   private String name;
   private String personalCode;
   private String email;
   private String address;

   private final static AtomicLong count = new AtomicLong(0);

    private final static String findStatementString =
            "SELECT Client_ID, Name, Personal_Code, Email, Address" +
                    " FROM Client" +
                    " WHERE Personal_Code = ?";

    private final static String populateDBString =
            "SELECT * FROM Client";

    private final static String updateStatementString =
            "UPDATE Client" +
                    " set Name = ?, Personal_Code = ?, Email = ?, Address = ?" +
                    " where Client_ID = ?";

    private final static String insertStatementString =
            "INSERT INTO Client VALUES (?, ?, ?, ?, ?)";

    public static void populateDb() {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement populateDBStmt = null;
        ResultSet rs = null;

        try {
            populateDBStmt = c.prepareStatement(populateDBString);
            rs = populateDBStmt.executeQuery();

            while(rs != null) {
                count.getAndAdd(1);
                rs.next();
                load(rs);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            try {
            if(rs != null)
                    rs.close();
            if(populateDBStmt != null)
                populateDBStmt.close();
                c.close();
                } catch(SQLException ex) {
                ex.printStackTrace();
            }
            }

    }

    public static Client find(String personalCodeString, String dbConnection) throws SQLException {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dbConnection);
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        Client result = BankRegistry.getClient(personalCodeString);
        if (result != null)
            return result;
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try {
            findStatement = c.prepareStatement(findStatementString);
            findStatement.setString(1, personalCodeString);
            rs = findStatement.executeQuery();
            rs.next();
            result = load(rs);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(rs != null)
            rs.close();
            if(findStatement != null)
            findStatement.close();
            c.close();

        }
        return result;
    }

    public static Client load(ResultSet rs) throws SQLException {
        String personalCode = rs.getString(3);
        Client result = BankRegistry.getClient(personalCode);
        if (result != null) return result;
        Long userID = new Long(rs.getLong(1));
        String name = rs.getString(2);
        String email = rs.getString(4);
        String address = rs.getString(5);
        result = new Client(userID, name, personalCode, email, address);
        BankRegistry.registerClient(result);
        return result;
    }

    public void update() throws SQLException {

        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement updateStatement = null;
        try {
            updateStatement = c.prepareStatement(updateStatementString);
            updateStatement.setString(1, name);
            updateStatement.setString(2, personalCode);
            updateStatement.setString(3, email);
            updateStatement.setString(4, address);
            updateStatement.setInt(5, getUserID().intValue());
            updateStatement.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(updateStatement != null)
            updateStatement.close();
            c.close();
        }
    }

    public Long insert(String dbConnection) throws SQLException {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection(dbConnection);
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement insertStatement = null;
        try {
            insertStatement = c.prepareStatement(insertStatementString);

            setUserID(count.incrementAndGet());
            insertStatement.setInt(1, getUserID().intValue());
            insertStatement.setString(2, name);
            insertStatement.setString(3, personalCode);
            insertStatement.setString(4, email);
            insertStatement.setString(5, address);
            insertStatement.execute();
            BankRegistry.registerClient(this);
            System.out.println(personalCode);
            return getUserID();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(insertStatement != null)
            insertStatement.close();
            c.close();
        }
        return getUserID();

    }

    public Client(Long userID, String name, String personalCode, String email, String address) {

        this.userID = userID;
        this.name = name;
        this.personalCode = personalCode;
        this.email = email;
        this.address = address;


    }

    public Client(String name, String personalCode, String email, String address) {

        this.name = name;
        this.personalCode = personalCode;
        this.email = email;
        this.address = address;


    }

    @Override
    public String toString() {
        return "Client{" +
                "userID=" + userID +
                ", name='" + name + '\'' +
                ", personalCode='" + personalCode + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        if (!getUserID().equals(client.getUserID())) return false;
        if (!name.equals(client.name)) return false;
        if (!getPersonalCode().equals(client.getPersonalCode())) return false;
        if (!email.equals(client.email)) return false;
        return address.equals(client.address);
    }

    @Override
    public int hashCode() {
        int result = getUserID().hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + getPersonalCode().hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + address.hashCode();
        return result;
    }

    public String getPersonalCode() {
        return personalCode;
    }


    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }


    public void setName(String name) {
        this.name = name;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public void setAddress(String address) {
        this.address = address;
    }
}
