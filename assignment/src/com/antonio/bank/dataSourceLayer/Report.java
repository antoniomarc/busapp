package com.antonio.bank.dataSourceLayer;

import com.antonio.bank.businessLogicLayer.BankRegistry;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Antonio on 3/28/2017.
 */
public class Report {

    private Long reportID;
    private String activity;
    private LocalDate activityDate;

    private final static AtomicLong count = new AtomicLong(0);

    private final static String findStatementString =
            "SELECT Report_ID,  Date, Activity, Personal_Code" +
                    " FROM Report" +
                    " WHERE Report_ID = ?";

    private final static String populateDBString =
            "SELECT * FROM Report";

    private final static String insertStatementString =
            "INSERT INTO Report VALUES (?, ?, ?, ?)";

    private final static String deleteStatementString =
            "DELETE FROM Report " +
                    " where Personal_Code = ?";

    private final static String generateReportStatementString =
            "SELECT * FROM Report" +
                    " WHERE Date BETWEEN ? AND ? AND Personal_Code = ?;";

    public Report(Long reportID, String activity, LocalDate activityDate) {
        this.reportID = reportID;
        this.activity = activity;
        this.activityDate = activityDate;
    }

    public Report(String activity, LocalDate activityDate) {
        this.activity = activity;
        this.activityDate = activityDate;
    }

    public static void populateDb() {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement populateDBStmt = null;
        ResultSet rs = null;

        try {
            populateDBStmt = c.prepareStatement(populateDBString);
            rs = populateDBStmt.executeQuery();

            while(rs != null) {
                count.getAndAdd(1);
                rs.next();
                load(rs);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(populateDBStmt != null)
                    populateDBStmt.close();
                c.close();
            } catch(SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    public static Report find(String id) throws SQLException {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        Report result = BankRegistry.getReport(id);
        if (result != null)
            return result;
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try {
            findStatement = c.prepareStatement(findStatementString);
            findStatement.setLong(1, Long.parseLong(id));
            rs = findStatement.executeQuery();
            rs.next();
            result = load(rs);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(rs != null)
            rs.close();
            if(findStatement != null)
            findStatement.close();
            //c.commit();
            c.close();
        }
        return null;
    }

    public static Report load(ResultSet rs) throws SQLException {

        Long reportID = new Long(rs.getLong(1));
        Report result = BankRegistry.getReport(reportID.toString());
        if (result != null) return result;

        LocalDate date = LocalDate.parse(rs.getString(2));
        String activity = rs.getString( 3);
        String personalCode = rs.getString(4);
        result = new Report(reportID, activity, date);
        BankRegistry.registerReport(personalCode, result);
        return result;
    }


    public Long insert(String personalCode) throws SQLException {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement insertStatement = null;
        try {
            insertStatement = c.prepareStatement(insertStatementString);

            setReportID(count.incrementAndGet());
            insertStatement.setInt(1, getReportID().intValue());
            insertStatement.setString(2, activityDate.toString());
            insertStatement.setString(3, activity);
            insertStatement.setString(4, personalCode);
            insertStatement.execute();
            System.out.println(personalCode);
            BankRegistry.registerReport(personalCode, this);
            System.out.println(personalCode);
            return getReportID();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(insertStatement != null)
            insertStatement.close();
            // c.commit();
            c.close();
        }
        return getReportID();

    }

    public static List<Report> generate(LocalDate from, LocalDate to, String personalCode) throws SQLException {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        List<Report> reports = new ArrayList<Report>();
        Report result;
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try {
            findStatement = c.prepareStatement(generateReportStatementString);
            findStatement.setString(1, from.toString());
            findStatement.setString(2, to.toString());
            findStatement.setString(3, personalCode);
            rs = findStatement.executeQuery();

            while(rs != null) {
                rs.next();
                result = load(rs);
                System.out.println(result.getActivity());
                reports.add(result);
            }
            return reports;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(rs != null)
                rs.close();
            if(findStatement != null)
                findStatement.close();
            c.close();
        }
        return reports;
    }

    public static void delete(String personalCode) {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = c.prepareStatement(deleteStatementString);
            deleteStatement.setString(1, personalCode);
            deleteStatement.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(deleteStatement != null)
                    deleteStatement.close();
                c.close();
            } catch(SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public Long getReportID() {
        return reportID;
    }

    public void setReportID(Long reportID) {
        this.reportID = reportID;
    }

    public String getActivity() {
        return activity;
    }

}
