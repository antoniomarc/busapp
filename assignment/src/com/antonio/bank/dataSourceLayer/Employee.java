package com.antonio.bank.dataSourceLayer;



import com.antonio.bank.businessLogicLayer.BankRegistry;

import java.sql.*;
import java.util.concurrent.atomic.AtomicLong;


/**
 * Created by Antonio on 3/20/2017.
 */
public class Employee {


    private final static AtomicLong count = new AtomicLong(0);

    private final static String findStatementString =
            "SELECT Employee_ID, Name, Personal_Code, Email, Adress, Password, AdminRights" +
                    " FROM Employee" +
                    " WHERE Personal_Code = ?";

    private final static String populateDBString =
            "SELECT * FROM Employee";

    private final static String updateStatementString =
            "UPDATE Employee" +
                    " set Name = ?, Personal_Code = ?, Email = ?, Address = ?, Password = ?, AdminRights = ?" +
                    " where Employee_ID = ?";

    private final static String insertStatementString =
            "INSERT INTO Employee VALUES (?, ?, ?, ?, ?, ?, ?)";

    private final static String deleteStatementString =
            "DELETE FROM Employee " +
                    " where Personal_Code = ?";


    private Long userID;
    private String name;
    private String personalCode;
    private String email;
    private String address;
    private String password;
    private Boolean adminRights;

    public Employee(Long userID, String name, String personalCode, String email, String address, String password, Boolean adminRights) {

        this.userID = userID;
        this.name = name;
        this.personalCode = personalCode;
        this.email = email;
        this.address = address;
        this.password = password;
        this.adminRights = adminRights;


    }

    public Employee(String name, String personalCode, String email, String address, String password, Boolean adminRights) {
        this.name = name;
        this.personalCode = personalCode;
        this.email = email;
        this.address = address;
        this.password = password;
        this.adminRights = adminRights;

    }

    public static void populateDb() {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement populateDBStmt = null;
        ResultSet rs = null;

        try {
            populateDBStmt = c.prepareStatement(populateDBString);
            rs = populateDBStmt.executeQuery();

            while(rs != null) {
                count.getAndAdd(1);
                rs.next();
                load(rs);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(rs != null)
                    rs.close();
                if(populateDBStmt != null)
                    populateDBStmt.close();
                c.close();
            } catch(SQLException ex) {
                ex.printStackTrace();
            }
        }

    }


    public static Employee find(String id) throws SQLException {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        Employee result = BankRegistry.getEmployee(id);
        if (result != null)
            return result;
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try {
            findStatement = c.prepareStatement(findStatementString);
            findStatement.setString(1, id);
            rs = findStatement.executeQuery();
            rs.next();
            result = load(rs);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
    } finally {
        if(rs != null)
        rs.close();
        if(findStatement != null)
        findStatement.close();
        c.close();
    }
        return result;
    }

    public static Employee load(ResultSet rs) throws SQLException {
        String personalCode = rs.getString(3);
        Employee result = BankRegistry.getEmployee(personalCode);
        if (result != null) return result;
        Long userID = new Long(rs.getLong(1));
        String name = rs.getString(2);
        String email = rs.getString(4);
        String address = rs.getString(5);
        String password = rs.getString(6);
        Boolean adminRights = rs.getInt(7) != 0;
        System.out.println(adminRights);
        result = new Employee(userID, name, personalCode, email, address, password, adminRights);
        BankRegistry.registerEmployee(result);
        return result;
    }

    public void update() throws SQLException {

        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement updateStatement = null;
        try {
            updateStatement = c.prepareStatement(updateStatementString);
            updateStatement.setString(1, name);
            updateStatement.setString(2, personalCode);
            updateStatement.setString(3, email);
            updateStatement.setString(4, address);
            updateStatement.setString(5, password);
            updateStatement.setBoolean(6, adminRights);
            updateStatement.setInt(7, getUserID().intValue());
            updateStatement.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(updateStatement != null)
            updateStatement.close();
            c.close();
        }
    }

    public Long insert() throws SQLException {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement insertStatement = null;
        try {
            insertStatement = c.prepareStatement(insertStatementString);

            setUserID(count.incrementAndGet());
            insertStatement.setInt(1, getUserID().intValue());
            insertStatement.setString(2, name);
            insertStatement.setString(3, personalCode);
            insertStatement.setString(4, email);
            insertStatement.setString(5, address);
            insertStatement.setString(6, password);
            insertStatement.setBoolean(7, adminRights);
            insertStatement.execute();
            BankRegistry.registerEmployee(this);
            return getUserID();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            insertStatement.close();
            c.close();
        }
        return getUserID();
    }

    public void delete() throws SQLException {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = c.prepareStatement(deleteStatementString);
            deleteStatement.setString(1, personalCode);
            deleteStatement.execute();
            Report.delete(this.getPersonalCode());
            BankRegistry.removeEmployee(this);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            deleteStatement.close();
            c.close();
        }
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getAdminRights() {
        return adminRights;
    }

    public void setAdminRights(Boolean adminRights) {
        this.adminRights = adminRights;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(String personalCode) {
        this.personalCode = personalCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "userID=" + userID +
                ", name='" + name + '\'' +
                ", personalCode='" + personalCode + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", password='" + password + '\'' +
                ", adminRights=" + adminRights +
                '}';
    }




    @Override
    public int hashCode() {
        int result = getUserID().hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + getPersonalCode().hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + getPassword().hashCode();
        result = 31 * result + getAdminRights().hashCode();
        return result;
    }

}
