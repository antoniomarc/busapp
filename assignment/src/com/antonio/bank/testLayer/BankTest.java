package com.antonio.bank.testLayer;

import com.antonio.bank.dataSourceLayer.Account;
import com.antonio.bank.dataSourceLayer.Client;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by Antonio on 4/3/2017.
 */
public class BankTest {

    private final static String createAccountTableStmt = "CREATE TABLE IF NOT EXISTS Account (" +
            "Account_ID INT PRIMARY KEY NOT NULL," +
            "    AccNo VARCHAR(45) NULL," +
            "    Type VARCHAR(45) NULL," +
            "    Money  VARCHAR(45) NULL," +
            "    Date  VARCHAR(45) NULL," +
            "    Personal_Code VARCHAR(45)," +
            "    FOREIGN KEY (Personal_Code) " +
            "       REFERENCES Client (Personal_Code)" +
            ");";

    private final static String createClientTableStmt = "CREATE TABLE IF NOT EXISTS Client (" +
            "Client_ID INT PRIMARY KEY NOT NULL," +
            "    Name VARCHAR(45) NULL," +
            "    Personal_Code VARCHAR(45) NULL," +
            "    Email  VARCHAR(45) NULL," +
            "    Address  VARCHAR(45) NULL" +
            ");";

    @Test
    public void testAddAccount() {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:junit.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement stmt = null;

        try {
            stmt = c.prepareStatement(createClientTableStmt);
            stmt.execute();
            stmt = c.prepareStatement(createAccountTableStmt);
            stmt.execute();
        } catch(SQLException e) {}
        finally{
            try {
                if (stmt != null)
                    stmt.close();
                c.close();
            } catch(SQLException ex) {}
        }

        Account newAccount = null;

        Client newClient = null;

        try {
            newClient = Client.find("123", "jdbc:sqlite:junit.db");
            if(newClient == null) {
                newClient = new Client("Test Name", "123", "mail", "address");
                newClient.insert("jdbc:sqlite:junit.db");
            }
           newAccount = Account.find("Test", "jdbc:sqlite:junit.db");
            if(newAccount == null) {
                newAccount = new Account("Test3", "Cont de economii", 10.0, LocalDate.now().toString());
                newAccount.insert("123","jdbc:sqlite:junit.db");
            }

            Account dbAccount = Account.find("Test3", "jdbc:sqlite:junit.db");
            assertEquals(dbAccount.getAccNo(), newAccount.getAccNo());
            assertEquals(dbAccount.getAccountType(), newAccount.getAccountType());
            assertEquals(dbAccount.getMoneyAmount(), newAccount.getMoneyAmount());
        } catch(SQLException e) {}
    }

    @Test
    public void testDeleteAccount() {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:junit.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }


        PreparedStatement stmt = null;

        try {
            stmt = c.prepareStatement(createClientTableStmt);
            stmt.execute();
            stmt = c.prepareStatement(createAccountTableStmt);
            stmt.execute();
        } catch(SQLException e) {}
        finally{
            try {
                if (stmt != null)
                    stmt.close();
                c.close();
            } catch(SQLException ex) {}
        }

        Account newAccount = null;
        Client newClient = null;
        try {
            newClient = Client.find("123", "jdbc:sqlite:junit.db");
            if (newClient == null) {
                newClient = new Client("Test Name", "123", "mail", "address");
                newClient.insert("jdbc:sqlite:junit.db");
            }

            newAccount = Account.find("Test1", "jdbc:sqlite:junit.db");
            if(newAccount == null) {
                newAccount = new Account("Test1", "Cont de economii", 12.0, LocalDate.now().toString());
                newAccount.insert(newClient.getPersonalCode(), "jdbc:sqlite:junit.db");
            }

            newAccount.delete(newClient.getPersonalCode(), "jdbc:sqlite:junit.db");

            Account dbAccount = Account.find("Test1", "jdbc:sqlite:junit.db");

            assertEquals(null, dbAccount);
        } catch(SQLException e) {}
    }

    @Test
    public void testTransaction() {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:junit.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }

        PreparedStatement stmt = null;

        try {
            stmt = c.prepareStatement(createClientTableStmt);
            stmt.execute();
            stmt = c.prepareStatement(createAccountTableStmt);
            stmt.execute();
        } catch(SQLException e) {}
        finally{
            try {
                if (stmt != null)
                    stmt.close();
                c.close();
            } catch(SQLException ex) {}
        }

        Account newAccount = null;
        Account secondAccount = null;
        Client newClient = null;
        try {

            newClient = Client.find("123", "jdbc:sqlite:junit.db");
                    if(newClient == null) {
                        newClient = new Client("Test Name", "123", "mail", "address");
                        newClient.insert("jdbc:sqlite:junit.db");
                    }
            newAccount = Account.find("Test11", "jdbc:sqlite:junit.db");
            if(newAccount == null) {
                newAccount = new Account("Test11", "Cont de economii", 10.0, LocalDate.now().toString());
                newAccount.insert(newClient.getPersonalCode(), "jdbc:sqlite:junit.db");
            }

            secondAccount = Account.find("Test21", "jdbc:sqlite:junit.db");
            if(secondAccount == null) {
                secondAccount = new Account("Test21", "Cont de economii", 15.0, LocalDate.now().toString());
                secondAccount.insert(newClient.getPersonalCode(), "jdbc:sqlite:junit.db");
            }
            Account.transferMoney(newClient.getPersonalCode(),newAccount.getAccNo(), secondAccount.getAccNo(),
                    5.0);

            assertEquals(5.0, newAccount.getMoneyAmount(), 0.1);
            assertEquals(20.0, secondAccount.getMoneyAmount(), 0.1);

        } catch(SQLException e) {}
    }
}
