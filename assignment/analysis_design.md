# Bank Application
# Analysis and Design Document

### Student: Marc Antonio-Sebastian
### Grupa: 30231


## Tabel of contents

#### 1. Requirements Analysis

##### 1.1 Assignment Specification

##### 1.2 Functional Requirements

##### 1.3 Non-functional Requirements

#### 2. Use-Case Model

#### 3. System Architectural Design

##### 3.1 Architectural Pattern Description

##### 3.2 Diagrams

#### 4. UML Sequence Diagrams

#### 5. Class Design

##### 5.1 Design Patterns Description
##### 5.2 UML Class Diagram

#### 6. Data Model

#### 7. System Testing

#### 8. Bibliography


#### 1. Requirements Analysis

##### 1.1 Assignment Specification

 The proposed assignment consists in creating an application for a bank using Java/C# API.
 The application should have two types of users (a regular user represented by the front
desk employee and an administrator user) which have to provide a username and a password in
order to use the application.

##### 1.2 Functional requirements

The regular user can perform the following operations:

* Add/update/view client information (name, identity card number, personal numerical code, address, etc.).

* Create/update/delete/view client account (account information: identification number, type, amount of money, date of creation).

* Transfer money between accounts.

* Process utilities bills.

The administrator user can perform the following operations:

* CRUD on employees information.

* Generate reports for a particular period containing the activities performed by an employee. 

Besides those constraints, the data has to be stored in a database, the application has to be organized using the Layers 
architectural pattern and also the data has to be processed using a domain logic pattern (transaction script or domain model) / a data
source hybrid pattern (table module, active record) and a data source pure pattern (table
data gateway, row data gateway, data mapper).

##### 1.3 Non-functional requirements

The input submitted by the user has to be validated against invalid data before being processed or saving it in the database.

#### 2. Use-Case Model

###### Use-Case Diagram

![Use-Case](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/ac2fb1836e5a266e61752d882612b29a2f3f2660/assignment/diagrams/assignment_usecase.JPG?token=0dca9c68fbfd07b0b03a163d7f31e8d238ddb291)


Description for adding client account use case:

Use case: <Add client account>

Level: <User-goal level>

Primary actor: <Regular user (employee)>

Main success scenario: <Login using username and password - Press Add account button - Wait for dialog box to open
- Enter account info - Input submitted is valid - Account is added to database>

Extensions: <User provides wrong username or password or invalid input for the account>

#### 3. System Architectural Design

##### 3.1 Architectural Pattern Description

The architectural pattern that was used in this application is called Layers and it separates the presentation, data processing and data storage
into three separate units called Data Access Layer (database), Business Logic Layer (data processing) and Presentation Layer (application UI).
The presentation layer is used to display information and to communicate with the user. Business Logic Layer is used to control application
functionality by processing the data submitted by the user or the data that is stored in the database. Data Access Layer is used to store the
data using persistance mechanisms like a database.

##### 3.2 Diagrams

###### Package diagram

![Package](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/ac2fb1836e5a266e61752d882612b29a2f3f2660/assignment/diagrams/assignment_package.JPG?token=1641c220b50f188f21d3a427cb108d3dd8659cc0)

###### Component diagram

![Component](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/ac2fb1836e5a266e61752d882612b29a2f3f2660/assignment/diagrams/assignment_component.JPG?token=21fa18308b3d92de53efacd3ed27bcb3efa85c11)

#### 4. UML Sequence diagram

![Sequence](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/ac2fb1836e5a266e61752d882612b29a2f3f2660/assignment/diagrams/assignment_sequence.JPG?token=2ed0144fbc4eb4bc2c538aa2fa5d1798d493d882)

#### 5. Class Design

##### 5.1 Design Patterns Description

The design patterns that I've used in the application are Active Record (Design source hybrid pattern) and Registry which is used in 
accordance with Active Record. Active Record is used to model the application in such way that an object carries both data and behaviour, 
in that case it is responsible for saving and loading data to the database and also for any domain logic that is used on it. Registry pattern
is used to access any object within the application.

##### 5.2 UML Class Diagram

![Class](https://bytebucket.org/antoniomarc/ps-30231-marcantonio/raw/ac2fb1836e5a266e61752d882612b29a2f3f2660/assignment/diagrams/assignment_class.JPG?token=fd26b6e212488f5576a24d5587b5e81327e6ba8d)

#### 6. Data Model

The data model that I've used in the application resembles the object-relational model which provides a middle ground between relational databases
and object databases, using the advantages of both fields (using queries to load and manipulate data from the database but also, being able
to use programming API in order to process the data that was loaded from the database).

#### 7. System Testing

The system was tested using Unit Testing with JUnit Framework and Data Flow Testing method. Also every functionality was tested from the UI by
validating against invalid input and checking the overall flow for all of the use cases.

#### 8. Bibliography

[1] Patterns of Enterprise Application Architecture - Martin Fowler

[[2]](https://en.wikipedia.org/wiki/Data_model) Wikipedia

[[3]](https://stackoverflow.com/) Stack Overflow
