# README #

In order to use the application, you have to install an IDE for Java (IntelliJ
or Eclipse) and DB Browser for SQLite. You can find the installer for IntelliJ IDEA [here](https://www.jetbrains.com/idea/download/#section=windows)
In order to install DB Browser for SQLite follow [this link](https://github.com/sqlitebrowser/sqlitebrowser/releases).
This program is needed in order to view the database and to execute the SQL script that populates the database.
To start the application, just run it using any IDE or from command prompt(terminal). After you run it a new file
"test.db" will be created which you can access using DB Browser for SQLite. Open the file and copy this SQL script:

INSERT INTO Employee VALUES 
            ('1', 'Marc Antonio', '1924762692431', 'antomarc19@yahoo.com', 'Nicolae Balcescu nr. 17', 'pass123', '1'), 
            ('2', 'Popescu Ion', '2428348952983', 'popescuion@gmail.com', '21 decembrie nr 23', 'pass1234', '0');
			
and paste it in the Execute SQL tab in order to populate the database with 2 employee accounts.
In order to use the whole functionality of the app, run it again and login using one of the created employee accounts,
after that you can use any option you want from the app. Each button describes the functionality it has when it's pressed.



